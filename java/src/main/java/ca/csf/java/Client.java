package ca.csf.java;

import ca.csf.hello.GreeterGrpc;
import ca.csf.hello.Hello;
import io.grpc.*;
import io.grpc.stub.MetadataUtils;

import java.util.Arrays;

public final class Client {

    // Point de départ du programme.
    public static void main(String[] args) {
        // Instanciation du client.
        var channel = ManagedChannelBuilder.forAddress("127.0.0.1", 5000).usePlaintext().build();
        var client = GreeterGrpc.newBlockingStub(channel);

        // Ajout d'authentification (si demandé).
        if (Arrays.asList(args).contains("--auth")) {
            // Simple message dans la console pour indiquer que l'authentification est bien activée.
            System.out.println("Authentification enabled");

            // En Java (et en Kotlin aussi), les métadonnées doivent être ajoutées via un interceptor.
            // C'est la seule plateforme où c'est fait ainsi (et ce n'est vraiment pas génial).
            client = client.withInterceptors(MetadataUtils.newAttachHeadersInterceptor(
                    MetadataUtil.newMetadataPair(MetadataUtil.AUTHORIZATION, "secret")
            ));
        }

        // Création de la requête.
        var request = Hello.GreetRequest.newBuilder()
                .setName("Java")
                .build();

        // Envoi de la requête.
        var response = client.greet(request);

        // Affichage de la réponse
        System.out.println(response.getResult());
    }

}
