package ca.csf.java;

import io.grpc.Metadata;

/**
 * Utilitaires pour la gestion des metadata.
 */
public final class MetadataUtil {

    /**
     * Metadata pour l'authentification.
     */
    public static final Metadata.Key<String> AUTHORIZATION = Metadata.Key.of("authorization", Metadata.ASCII_STRING_MARSHALLER);

    /**
     * Constructeur privé pour empêcher l'instanciation de cette classe utilitaire.
     */
    private MetadataUtil() {
        // Privé pour empêcher l'instanciation.
    }

    public static Metadata newMetadataPair(Metadata.Key<String> key, String value) {
        var metadata = new Metadata();
        metadata.put(key, value);
        return metadata;
    }
}
