package ca.csf.java;

import ca.csf.hello.GreeterGrpc;
import ca.csf.hello.Hello;
import io.grpc.*;
import io.grpc.protobuf.services.ProtoReflectionServiceV1;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static ca.csf.java.ExceptionUtil.expect;

public final class Server {

    // Point de départ du programme.
    public static void main(String[] args) throws IOException {
        // Configuration du serveur.
        var builder = ServerBuilder
                .forPort(5000)
                .addService(new GreeterService());

        // Ajout d'authentification (si demandé).
        if (Arrays.asList(args).contains("--auth")) {
            System.out.println("Authentification enabled");
            builder.intercept(new AuthInterceptor(new String[]{"secret"}));
        }

        // Ajout du service de reflection gRPC (seulement si on en est mode développement).
        if (System.getenv("dev") != null) {
            builder.addService(ProtoReflectionServiceV1.newInstance());
        }

        // Création du serveur.
        var server = builder.build();

        // Ceci s'assure de fermer le serveur en même temps que la JVM.
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            expect(() -> server.shutdown().awaitTermination(), "server should stop without issue");
        }));

        // Démarrage du serveur.
        System.out.println("Server is running");
        expect(() -> server.start().awaitTermination(), "server should start without issue");
    }

    // Implémentation du service Greeter.
    private static final class GreeterService extends GreeterGrpc.GreeterImplBase {
        @Override
        public void greet(Hello.GreetRequest request, StreamObserver<Hello.GreetResponse> responseObserver) {
            // Validation de la requête.
            var name = request.getName().trim();
            if (name.isEmpty()) {
                // Retour d'une erreur avec un code 3 (INVALID_ARGUMENT). Pour la liste complète des
                // codes de retour, voir ceci : https://grpc.io/docs/guides/status-codes/
                //
                // Note : Java n'a pas de fonctions async. Il faut donc lancer les erreurs avec "onError".
                responseObserver.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("name cannot be empty")));
                return;
            }

            // Création de la réponse.
            var response = Hello.GreetResponse.newBuilder()
                    .setResult(String.format("Hello %s from Java!", name))
                    .build();

            // Envoi de la réponse.
            //
            // Note : Java n'a pas de fonctions async. Il faut donc répondre avec "onNext". La fonction s'appelle ainsi,
            //        car il s'agit d'une chaine de responsabilités.
            responseObserver.onNext(response);

            // Note : Java n'a pas de fonctions async. Il faut terminer l'envoie de la réponse avec "onCompleted".
            responseObserver.onCompleted();
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static final class AuthInterceptor implements ServerInterceptor {
        private static final ServerCall.Listener EMPTY_CALL_HANDLER = new ServerCall.Listener<>() {};

        private final List<String> secrets;

        public AuthInterceptor(String[] secrets) {
            this.secrets = Arrays.asList(secrets);
        }

        @Override
        public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
                ServerCall<ReqT, RespT> call,
                Metadata headers,
                ServerCallHandler<ReqT, RespT> next
        ) {
            // Tente d'obtenir la métadonnée "authorization" (équivalent d'un header).
            var metadata = headers.get(MetadataUtil.AUTHORIZATION);

            // Si la métadonnée n'est n'est pas présente, produire une erreur 3 (INVALID_ARGUMENT).
            if (metadata == null)
                return error(call, Status.INVALID_ARGUMENT.withDescription("missing authorization metadata"));

            // Faire l'authentification. Nous faisons une recherche dans un tableau, mais il pourrait y avoir une base de données.
            if (!secrets.contains(metadata))
                return error(call, Status.UNAUTHENTICATED.withDescription("invalid authorization metadata"));

            // Sinon, continuer (chain of responsibility).
            return next.startCall(call, headers);
        }

        private <ReqT, RespT> ServerCall.Listener<ReqT> error(ServerCall<ReqT, RespT> call, Status status) {
            call.close(status, new Metadata());
            return EMPTY_CALL_HANDLER;
        }
    }
}
