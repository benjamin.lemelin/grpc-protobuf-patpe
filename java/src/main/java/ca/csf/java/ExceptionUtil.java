package ca.csf.java;

/**
 * Utilitaires pour la gestion des exceptions.
 */
public final class ExceptionUtil {
    /**
     * Constructeur privé pour empêcher l'instanciation de cette classe utilitaire.
     */
    private ExceptionUtil() {
        // Privé pour empêcher l'instanciation.
    }

    /**
     * Exécute un Runnable et renvoie son résultat, ou lance une RuntimeException
     * avec le message spécifié en cas d'exception.
     * <p>
     * Cette méthode est pratique si vous présumez qu'une exception ne devrait jamais se produire, et que vous ne voulez
     * pas faire de {@code try/catch}.
     *
     * @param runnable Runnable à exécuter.
     * @param message  Message à inclure dans la RuntimeException si une exception est levée.
     * @throws RuntimeException Si une exception est levée lors de l'exécution du Callable.
     */
    public static void expect(ExpectRunnable runnable, String message) {
        try {
            runnable.run();
        } catch (Exception e) {
            throw new RuntimeException(message, e);
        }
    }

    /**
     * Exécute un Callable et renvoie son résultat, ou lance une RuntimeException
     * avec le message spécifié en cas d'exception.
     * <p>
     * Cette méthode est pratique si vous présumez qu'une exception ne devrait jamais se produire, et que vous ne voulez
     * pas faire de {@code try/catch}.
     *
     * @param callable Callable à exécuter.
     * @param message  Message à inclure dans la RuntimeException si une exception est levée.
     * @param <T>      Type de résultat retourné par le Callable.
     * @return Résultat du Callable.
     * @throws RuntimeException Si une exception est levée lors de l'exécution du Callable.
     */
    public static <T> T expect(ExpectCallable<T> callable, String message) {
        try {
            return callable.call();
        } catch (Exception e) {
            throw new RuntimeException(message, e);
        }
    }

    /**
     * Tâche pouvant produire une exception.
     */
    public interface ExpectRunnable {
        void run() throws Exception;
    }

    /**
     * Tâche pouvant produire une exception, et retournant une information.
     */
    public interface ExpectCallable<T> {
        T call() throws Exception;
    }
}
