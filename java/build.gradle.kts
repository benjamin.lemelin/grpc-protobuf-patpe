val javaxVersion = "1.3.2"
val protobufVersion = "4.29.1"
val grpcVersion = "1.68.2"
val grpcNettyVersion = "1.68.2"
val grpcJavaVersion = "1.70.0"

plugins {
    id("java")
    id("com.google.protobuf") version "0.9.4"
}

group = "ca.csf"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation("javax.annotation:javax.annotation-api:$javaxVersion")
    implementation("com.google.protobuf:protobuf-java:$protobufVersion")
    implementation("io.grpc:grpc-api:$grpcVersion")
    implementation("io.grpc:grpc-protobuf:$grpcVersion")
    implementation("io.grpc:grpc-services:$grpcVersion")
    implementation("io.grpc:grpc-netty:$grpcNettyVersion")
    implementation("io.grpc:grpc-stub:$grpcJavaVersion")
}

sourceSets {
    main {
        proto {
            srcDir("../.proto")
            include("../.proto")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
    plugins {
        create("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                create("grpc")
            }
        }
    }
}

task("server", JavaExec::class) {
    environment("dev", "true")

    mainClass = "ca.csf.java.Server"
    classpath = sourceSets["main"].runtimeClasspath
}

task("client", JavaExec::class) {
    environment("dev", "true")

    mainClass = "ca.csf.java.Client"
    classpath = sourceSets["main"].runtimeClasspath
}

task("server-with-auth", JavaExec::class) {
    args("--auth")
    environment("dev", "true")

    mainClass = "ca.csf.java.Server"
    classpath = sourceSets["main"].runtimeClasspath
}

task("client-with-auth", JavaExec::class) {
    args("--auth")
    environment("dev", "true")

    mainClass = "ca.csf.java.Client"
    classpath = sourceSets["main"].runtimeClasspath
}