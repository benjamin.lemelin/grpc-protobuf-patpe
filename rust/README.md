![Logo Rust](../.docs/rust-logo.svg)

# Rust

Ce projet expérimente avec GRPC et Protobuf en Rust.

## Démarrage rapide

Ce projet nécessite d'installer le [Compilateur Rust][Rust Toolchain] sur votre machine. Veuillez suivre les 
instructions en fonction de votre système d'exploitation. Si vous aviez déjà le compilateur Rust sur votre machine, 
il vous est recommandé de le mettre à jour à sa dernière version avant de débuter.

```shell
rustup update
```

### Windows

Installez premièrement les outils de development C++ fournis avec [Visual Studio]. Ensuite, téléchargez et exécutez 
l'installateur [Rustup][Rust Toolchain]. Suivez les instructions.

### Ubuntu et dérivés

Installez les outils de développement *C++* : 

```shell
sudo apt-install build-essential
```

Ensuite, exécutez le script d'installation de [Rustup][Rust Toolchain] et suivez les instructions :

```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Fedora

Installez les outils de développement *C++* :

```shell
sudo dnf group install development-tools
```

Ensuite, exécutez le script d'installation de [Rustup][Rust Toolchain] et suivez les instructions :

```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### MacOs

Exécutez le script d'installation de [Rustup][Rust Toolchain] et suivez les instructions :

```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Exécuter le projet

Exécutez cette commande à l'intérieur du dossier du projet pour démarrer le serveur :

```shell
cargo run
```

Dans un nouveau terminal, exécutez cette commande ci-dessous pour démarrer un client qui fera une requête au serveur :

```shell
cargo run --bin client
```

## Exécuter le projet avec authentification

Ce projet contient trois exemples d'implémentation de l'authentification : 

* `Auth` : Authentification simple à même le service.
* `Auth-Interceptor` : Authentification utilisant un [Interceptor][gRPC Interceptor].
* `Auth-Layer` : Authentification utilisant un [Layer][Tower Layer].

Dans les trois cas, ils utilisent les métadonnées (sortes de *headers*) du paquet gRPC. C'est donc similaire à ce qui
se fait dans un service REST plus traditionnel. Pour exécuter un serveur, exécutez la commande `cargo run` en prenant
soit de préciser l'implémentation souhaitée. Par exemple :

```shell
cargo run --features auth              # Authentification à même le service.
cargo run --features auth-interceptor  # Authentification avec un Interceptor.
cargo run --features auth-layer        # Authentification avec un Layer.
```

Vous pouvez ensuite exécuter un client dans un autre terminal :

```shell
cargo run --bin client --features auth
```

## Environnements de développement

Il est recommandé d'utiliser [RustRover] en guise d'IDE (qui est désormais gratuit pour un usage non commercial). Il
existe aussi [Visual Studio Code] avec l'extension [rust-analyzer] (qui est totalement Open-Source).

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../LICENSE.md) pour les détails.

[Visual Studio]: https://visualstudio.microsoft.com/
[Rust Toolchain]: https://www.rust-lang.org/tools/install
[RustRover]: https://www.jetbrains.com/rust/
[Visual Studio Code]: https://code.visualstudio.com/
[rust-analyzer]: https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer
[gRPC Interceptor]: https://grpc.io/docs/guides/interceptors/
[Tower Layer]: https://docs.rs/tower/latest/tower/trait.Layer.html