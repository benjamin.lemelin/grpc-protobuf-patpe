use std::error::Error;
use std::{env, path::PathBuf};

// Compile le fichier `.proto` pour le projet. Ce script est exécuté en même temps que la
// compilation du programme. Cela permet de s'assurer que le code est synchronisé avec les
// fichiers `.proto`.

fn main() -> Result<(), Box<dyn Error>> {
    // La variable "OUT_DIR" est fourni par Cargo (le build system pour Rust). Il s'agit du chemin
    // vers le dossier où tout code généré doit être écrit.
    let out_dir = PathBuf::from(env::var("OUT_DIR").expect("Variable should be provided by Cargo"));

    // Exécute la compilation des fichiers ".proto". Cette commande s'occupe aussi de créer les
    // descripteurs pour la reflection gRPC.
    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("hello_descriptor.bin"))
        .compile_protos(&["../.proto/hello.proto"], &["../.proto"])?;

    Ok(())
}