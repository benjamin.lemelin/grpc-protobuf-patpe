use tonic::service::Interceptor;
use tonic::Status;

// Les Interceptor sont placés avant le service gRPC à protéger, et peuvent accepter ou refuser la
// requête (d'où le nom). Ils sont relativement simple à implémenter, mais ils ne peuvent pas
// contenir de code asynchrone, ce qui les rend très peu utiles en réalité.

/// Authentification avec un Interceptor.
#[derive(Debug, Copy, Clone)]
pub struct AuthInterceptor {
    secrets: &'static [&'static str],
}

impl AuthInterceptor {
    pub fn new(secrets: &'static [&'static str]) -> Self {
        Self {
            secrets
        }
    }
}

// Implémentation du trait Interceptor. Ce dernier reçoit la requête et la retourne directement s'il
// l'accepte. Il peut sinon retourner un Status et refuser la requête.
impl Interceptor for AuthInterceptor {
    fn call(&mut self, request: tonic::Request<()>) -> Result<tonic::Request<()>, Status> {
        match request.metadata().get("authorization") {
            Some(metadata) => {
                match metadata.to_str() {
                    Ok(metadata) => {
                        if self.secrets.contains(&metadata) {
                            Ok(request)
                        } else {
                            Err(Status::unauthenticated("invalid authorization token"))
                        }
                    }
                    Err(_) => {
                        Err(Status::invalid_argument("malformed authorization token"))
                    }
                }
            }
            None => {
                Err(Status::invalid_argument("missing authorization token"))
            }
        }
    }
}