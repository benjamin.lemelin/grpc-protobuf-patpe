use tonic::Status;

// Il est souvent recommandé de créer un Interceptor ou un Layer (sortes de middleware) pour
// effectuer de l'authentification avec gRPC. Ces deux méthodes ont leurs pour et leurs contre, mais
// elles ne sont pas nécessairement meilleures qu'une solution manuelle plus simple. C'est ce que
// cet exemple veut montrer : une manière simple et rapide d'authentifier l'utilisateur et qui ne
// nécessite pas beaucoup de travail.

/// Authentification de base.
#[derive(Debug)]
pub struct AuthBasic {
    secrets: &'static [&'static str],
}

impl AuthBasic {
    // Constructeur.
    pub fn new(secrets: &'static [&'static str]) -> Self {
        Self {
            secrets
        }
    }

    pub async fn authenticate<ReqBody>(&self, request: &tonic::Request<ReqBody>) -> Result<(), Status> {
        // Tente d'obtenir la métadonnée "authorization" (équivalent d'un header).
        match request.metadata().get("authorization") {
            // Si la métadonnée est présente, valide le token.
            Some(metadata) => {
                // Les métadonnées en gRPC peuvent être soit une chaine de caractères ASCII, soit un
                // tableau de bytes. Il faut donc essayer de faire une conversion en premier lieu.
                match metadata.to_str() {
                    Ok(metadata) => {
                        // Faire l'authentification en tant que telle. Ici, nous faisons une simple recherche
                        // dans un tableau, mais il pourrait y avoir un appel à une base de données.
                        if self.secrets.contains(&metadata) {
                            Ok(())
                        } else {
                            Err(Status::unauthenticated("invalid authorization metadata"))
                        }
                    }
                    Err(_) => {
                        Err(Status::invalid_argument("malformed authorization metadata"))
                    }
                }
            }
            None => {
                Err(Status::invalid_argument("missing authorization metadata"))
            }
        }
    }
}