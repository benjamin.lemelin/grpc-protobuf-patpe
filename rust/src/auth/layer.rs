use std::convert::Infallible;
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};

use tonic::metadata::MetadataMap;
use tonic::server::NamedService;
use tonic::Status;
use tower::{Service, Layer};

// Les Layers encapsulent le service gRPC à protéger, et peuvent faire des actions avant et après
// l'exécution de la requête. Leur seul désavantage est qu'ils sont très difficiles à implémenter
// en Rust, principalement à cause de l'asynchrone qui doit être implémentée manuellement.

// Layer d'authentification. Le Layer n'est pas le middleware à proprement parler. Il s'agit plutôt
// d'une fabrique de middleware. Lorsque le serveur est créé, ce dernier demande au Layer de créer
// un middleware encapsulant un autre service.
#[derive(Debug, Clone)]
pub struct AuthLayer {
    secrets: &'static [&'static str],
}

impl AuthLayer {
    // Constructeur.
    pub fn new(secrets: &'static [&'static str]) -> Self {
        Self {
            secrets
        }
    }
}

// Implémentation du trait Layer. La méthode Layer est appellée par le serveur pour créer le service
// d'authentification qui encapsule le service à protéger.
impl<S> Layer<S> for AuthLayer {
    type Service = AuthService<S>;

    fn layer(&self, inner: S) -> Self::Service {
        AuthService::new(self.secrets, inner)
    }
}

// Service d'authentification. Il s'agit du middleware, et contient donc le service à encapsuler.
#[derive(Debug, Clone)]
pub struct AuthService<S> {
    secrets: &'static [&'static str],
    inner: S,
}

impl<S> AuthService<S> {
    // Constructeur.
    pub fn new(secrets: &'static [&'static str], inner: S) -> Self {
        Self {
            secrets,
            inner,
        }
    }

    // Méthode effectuant l'authentification. Elle est appelée par l'implémentation du Service.
    async fn authenticate(
        secrets: &'static [&'static str],
        request: &tonic::Request<()>,
    ) -> Result<(), Status> {
        match request.metadata().get("authorization") {
            Some(metadata) => {
                match metadata.to_str() {
                    Ok(metadata) => {
                        if secrets.contains(&metadata) {
                            Ok(())
                        } else {
                            Err(Status::unauthenticated("invalid authorization token"))
                        }
                    }
                    Err(_) => {
                        Err(Status::invalid_argument("malformed authorization token"))
                    }
                }
            }
            None => {
                Err(Status::invalid_argument("missing authorization token"))
            }
        }
    }
}

// Implémentation du trait Service. Il s'agit de la partie complexe des Layers, car elle nécessite
// d'implémenter le code asynchrone manuellement (et encore, cette version a été simplifiée).
impl<S, ReqBody, ResBody> Service<http::Request<ReqBody>> for AuthService<S>
where
    S: Service<http::Request<ReqBody>, Response=http::Response<ResBody>, Error=Infallible> + Clone + Send + 'static,
    S::Future: Send + 'static,
    ReqBody: Send + 'static,
    ResBody: Default,
{
    type Response = S::Response;
    type Error = Infallible;
    type Future = Pin<Box<dyn Future<Output=Result<Self::Response, Self::Error>>+ Send + 'static>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx).map_err(Into::into)
    }

    fn call(&mut self, req: http::Request<ReqBody>) -> Self::Future {
        Box::pin({
            let secrets = self.secrets;
            let mut inner = self.inner.clone();

            async move {
                let uri = req.uri().clone();
                let method = req.method().clone();
                let version = req.version();
                let (parts, body) = req.into_parts();

                let mut req = tonic::Request::new(());
                *req.metadata_mut() = MetadataMap::from_headers(parts.headers);
                *req.extensions_mut() = parts.extensions;

                match Self::authenticate(secrets, &req).await {
                    Ok(_) => {
                        let (metadata, extensions, _) = req.into_parts();

                        let mut req = http::Request::new(body);
                        *req.uri_mut() = uri;
                        *req.method_mut() = method;
                        *req.version_mut() = version;
                        *req.headers_mut() = metadata.into_headers();
                        *req.extensions_mut() = extensions;

                        inner.call(req).await.map_err(Into::into)
                    }
                    Err(e) => {
                        Ok(e.into_http().map(|_| ResBody::default()))
                    }
                }
            }
        })
    }
}

// Tous les services Tonic doivent avoir un nom. C'est pareil pour les middlewares.
impl<S> NamedService for AuthService<S>
where
    S: NamedService,
{
    const NAME: &'static str = S::NAME;
}