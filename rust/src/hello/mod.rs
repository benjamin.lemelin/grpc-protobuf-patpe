// Cette instruction ajoute au module le code généré à partir du fichier `hello.proto`.
tonic::include_proto!("hello");

// File Descriptor Set pour la reflection gRPC.
#[allow(unused)]
pub const FILE_DESCRIPTOR_SET: &[u8] = tonic::include_file_descriptor_set!("hello_descriptor");
