use std::error::Error;

use tonic::transport::Server;
use tonic::{Request, Response, Status};
use tonic_reflection::server::{ServerReflection, ServerReflectionServer};
use tonic_web::GrpcWebLayer;
use tower_http::trace::TraceLayer;
use hello::greeter_server::{Greeter, GreeterServer};
use hello::{GreetRequest, GreetResponse};

#[cfg(feature = "auth-basic")]
use auth::basic::AuthBasic;
#[cfg(feature = "auth-interceptor")]
use auth::interceptor::AuthInterceptor;
#[cfg(feature = "auth-layer")]
use auth::layer::AuthLayer;

// Ce module contient le code auto-généré pour gRPC.
mod hello;
// Ce module contient les différentes implémentations de l'authentification.
mod auth;

// Cette structure représentera notre implémentation du service Greeter défini dans le
// fichier `.proto`. Nous allons l'implémenter justement un peu plus bas.
#[derive(Debug)]
struct GreeterService {
    #[cfg(feature = "auth-basic")]
    authenticator: AuthBasic
}

// Méthodes pour notre service.
impl GreeterService {
    // Constructeur de GreeterService.
    #[cfg(not(feature = "auth-basic"))]
    fn new() -> Self {
        Self {}
    }

    // Constructeur de GreeterService avec un authentificateur.
    #[cfg(feature = "auth-basic")]
    fn new(authenticator: AuthBasic) -> Self {
        Self {
            authenticator
        }
    }
}

// Implémentation du service Greeter.
#[tonic::async_trait]
impl Greeter for GreeterService {
    // Ceci est la fonction gérant l'appel GRPC du service. Remarquez que ce sont les mêmes
    // paramètres que dans le fichier `.proto`.
    async fn greet(
        &self,
        request: Request<GreetRequest>,
    ) -> Result<Response<GreetResponse>, Status> {
        // Si l'authentification de base (à même le service) est activée.
        #[cfg(feature = "auth-basic")]
        {
            // Il est possible de faire une authentification de base à même le service. Remarquez le
            // point d'interrogation (?) à la fin. Si l'authentification échoue, l'erreur est
            // propagée et la requête n'ira pas plus loin.
            self.authenticator.authenticate(&request).await?;
        }

        // Obtention du message.
        // L'objet "Request" contient d'autres informations (comme des métadonnées entre autres).
        let request = request.get_ref();

        // Validation de la requête.
        let name = request.name.trim();
        if name.is_empty() {
            // Retour d'une erreur avec un code 3 (INVALID_ARGUMENT). Pour la liste complète des
            // codes de retour, voir ceci : https://grpc.io/docs/guides/status-codes/
            return Err(Status::invalid_argument("name cannot be empty"));
        }

        // Création de la réponse.
        let response = GreetResponse {
            result: format!("Hello {} from Rust!", name),
        };

        // Envoi de la réponse. Elle est entourée d'un objet `Response` pour Tonic.
        Ok(Response::new(response))
    }
}

// Point d'entrée du programme.
// En temps normal, un point d'entrée en Rust n'est pas asynchrone, mais il est possible d'ajouter
// la macro `tokio::main` pour le rendre asynchrone. Ainsi, nous pouvons "await" sur des `Futures`.
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Charge le contenu du fichier env, s'il existe.
    dotenvy::dotenv().ok();

    // Activation de la journalisation.
    tracing_subscriber::fmt::init();

    // Si l'authentification n'est pas activée.
    #[cfg(not(any(feature = "auth-basic", feature = "auth-interceptor", feature = "auth-layer")))]
    let service = {
        // Création du service gRPC.
        GreeterServer::new(GreeterService::new())
    };

    // Si l'authentification est activée.
    #[cfg(feature = "auth")]
    println!("Authentification enabled");

    // Si l'authentification de base (à même le service) est activée.
    #[cfg(feature = "auth-basic")]
    let service = {
        // Création de l'authentification. Les identifiants sont codées ici en dur, mais ils pourraient
        // être obtenus à partir d'une base de données.
        let authenticator = AuthBasic::new(&["secret"]);

        // Service gRPC que nous voulons protéger. Nous lui envoyons son authentificateur.
        GreeterServer::new(GreeterService::new(authenticator))
    };

    // Si l'authentification avec un interceptor est activée.
    #[cfg(feature = "auth-interceptor")]
    let service = {
        // Interceptor pour l'authentification.
        let interceptor = AuthInterceptor::new(&["secret"]);

        // Service gRPC que nous voulons protéger.
        let service = GreeterService::new();

        // Construction du service avec son interceptor.
        GreeterServer::with_interceptor(service, interceptor)
    };

    // Si l'authentification avec un layer est activée.
    #[cfg(feature = "auth-layer")]
    let service = {
        // Création du Layer d'authentification
        let auth_layer = AuthLayer::new(&["secret"]);

        // Service gRPC que nous voulons protéger.
        let service = GreeterService::new();

        // Construction du service avec le Layer.
        tower::ServiceBuilder::new().layer(auth_layer).service(GreeterServer::new(service))
    };

    // Instanciation du server Tonic.
    let server = Server::builder();

    // Journalisation des requêtes (pour le débogage).
    let server = server.layer(TraceLayer::new_for_grpc());

    // Activation du protocole gRPC pour le web (avec Http/1 en plus de Http/2).
    let mut server = server.accept_http1(true).layer(GrpcWebLayer::new());

    // Ajout d'un GreeterService au serveur.
    let server = server.add_service(service);

    // Ajout du service de reflection gRPC (seulement si on en est mode débogage).
    #[cfg(debug_assertions)]
    let server = server.add_service(create_reflexion_service());

    // Écoute sur le port 5000.
    let app = server.serve("0.0.0.0:5000".parse()?);

    // Attend la fin de l'exécution du server.
    println!("Server is running");
    app.await?;

    // Fin du programme.
    Ok(())
}

// Méthode pour la création du service de reflection gRPC.
fn create_reflexion_service() -> ServerReflectionServer<impl ServerReflection> {
    // Builder du service de reflection.
    tonic_reflection::server::Builder::configure()
        // Les données sur la reflection (le "File Descriptor Set") sont dans une constante auto-générée.
        .register_encoded_file_descriptor_set(hello::FILE_DESCRIPTOR_SET)
        // Création du service.
        .build_v1()
        // Il ne devrait pas y avoir d'erreur lors du décodage du "File Descriptor Set".
        .expect("File descriptor set should be valid")
}