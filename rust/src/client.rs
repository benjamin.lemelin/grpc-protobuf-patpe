use std::error::Error;

use tonic::Request;

use hello::greeter_client::GreeterClient;
use hello::GreetRequest;

// Ce module contient le code auto-généré pour gRPC.
mod hello;

// Point d'entrée du programme.
// En temps normal, un point d'entrée en Rust n'est pas asynchrone, mais il est possible d'ajouter
// la macro `tokio::main` pour le rendre asynchrone. Ainsi, nous pouvons "await" sur des `Futures`.
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Instanciation du client.
    let mut client = GreeterClient::connect("http://127.0.0.1:5000").await?;

    // Création de la requête.
    #[allow(unused_mut)]
    let mut request = Request::new(GreetRequest { name: "Rust".to_owned() });

    // Ajout d'authentification (si demandé).
    #[cfg(feature = "auth")]
    {
        println!("Authentification enabled");
        request.metadata_mut().insert("authorization", "secret".parse().expect("should be valid"));
    }

    // Envoi de la requête.
    let response = client.greet(request).await;

    // Affichage de la réponse (en format debug pour simplifier).
    match response {
        Ok(response) => println!("{}", response.get_ref().result),
        Err(e) => eprintln!("Error : {}", e.message()),
    }

    // Fin du programme.
    Ok(())
}