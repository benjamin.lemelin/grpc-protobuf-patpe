![Logo Kotlin](../.docs/kotlin-logo.svg)

# Kotlin

Ce projet expérimente avec GRPC et Protobuf en Kotlin.

## Démarrage rapide

Ce projet nécessite d'installer le [Java Development Kit][JDK] sur votre machine. Veuillez suivre les instructions en 
fonction de votre système d'exploitation.

### Windows

Installez le [JDK Oracle] avec le gestionnaire de paquets [WinGet] :

```shell
winget install Oracle.JDK.23
```

### Ubuntu et dérivés

Installez la dernière version de [OpenJDK][JDK] avec votre gestionnaire de paquets :

```shell
sudo apt install openjdk-23-jdk
```

### Fedora

Installez la dernière version de [OpenJDK][JDK] avec votre gestionnaire de paquets :

```shell
sudo dnf install java-latest-openjdk-devel
```

### MacOs

Utilisez [Homebrew] pour installer la dernière version de [OpenJDK][JDK] :

```shell
brew install openjdk
```

## Exécuter le projet

Exécutez cette commande à l'intérieur du dossier du projet pour démarrer le serveur :

```shell
./gradlew server
```

Dans un nouveau terminal, exécutez cette commande ci-dessous pour démarrer un client qui fera une requête au serveur :

```shell
./gradlew client
```

Notez que si vous êtes sur Windows, vous aurez à utiliser `gradlew.bat` au lieu de `gradlew`.

## Exécuter le projet avec authentification

Ajoutez le paramètre `--auth` pour activer l'authentification :

```shell
./gradlew server-with-auth
```

Faites la même chose pour le client :

```shell
./gradlew client-with-auth
```

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../LICENSE.md) pour les détails.

[Kotlin]: https://kotlinlang.org/
[JDK]: https://openjdk.org/
[JDK Oracle]: https://www.oracle.com/java/technologies/downloads/
[WinGet]: https://github.com/microsoft/winget-cli
[Homebrew]: https://brew.sh/