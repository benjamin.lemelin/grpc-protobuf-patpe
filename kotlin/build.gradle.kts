val kotlinxVersion = "1.9.0"
val protobufVersion = "4.29.1"
val grpcVersion = "1.68.2"
val grpcNettyVersion = "1.68.2"
val grpcKotlinVersion = "1.4.1"

plugins {
    kotlin("jvm") version "2.1.10"
    id("com.google.protobuf") version "0.9.4"
}

group = "ca.csf"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-guava:$kotlinxVersion")
    implementation("com.google.protobuf:protobuf-kotlin:$protobufVersion")
    implementation("io.grpc:grpc-protobuf:$grpcVersion")
    implementation("io.grpc:grpc-services:$grpcVersion")
    implementation("io.grpc:grpc-netty:$grpcNettyVersion")
    implementation("io.grpc:grpc-kotlin-stub:$grpcKotlinVersion")
}

sourceSets {
    main {
        proto {
            srcDir("../.proto")
            include("../.proto")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
    plugins {
        create("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
        create("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpcKotlinVersion:jdk8@jar"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                create("grpc")
                create("grpckt")
            }
            it.builtins {
                create("kotlin")
            }
        }
    }
}

task("server", JavaExec::class) {
    environment("dev", "true")

    mainClass = "ca.csf.kotlin.ServerKt"
    classpath = sourceSets["main"].runtimeClasspath
}

task("client", JavaExec::class) {
    environment("dev", "true")

    mainClass = "ca.csf.kotlin.ClientKt"
    classpath = sourceSets["main"].runtimeClasspath
}

task("server-with-auth", JavaExec::class) {
    args("--auth")
    environment("dev", "true")

    mainClass = "ca.csf.kotlin.ServerKt"
    classpath = sourceSets["main"].runtimeClasspath
}

task("client-with-auth", JavaExec::class) {
    args("--auth")
    environment("dev", "true")

    mainClass = "ca.csf.kotlin.ClientKt"
    classpath = sourceSets["main"].runtimeClasspath
}