package ca.csf.kotlin

import kotlinx.coroutines.guava.await
import ca.csf.hello.GreeterGrpc
import ca.csf.hello.greetRequest
import io.grpc.ManagedChannelBuilder
import io.grpc.Metadata
import io.grpc.stub.MetadataUtils

// Point d'entrée du programme.
suspend fun main(args: Array<String>) {
    // Instanciation du client.
    val channel = ManagedChannelBuilder.forAddress("127.0.0.1", 5000).usePlaintext().build()
    var client = GreeterGrpc.newFutureStub(channel)

    // Ajout d'authentification (si demandé).
    if (args.contains("--auth"))
    {
        // Simple message dans la console pour indiquer que l'authentification est bien activée.
        println("Authentification enabled")

        // En Java (et en Kotlin aussi), les métadonnées doivent être ajoutées via un interceptor.
        // C'est la seule plateforme où c'est fait ainsi (et ce n'est vraiment pas génial).
        client = client.withInterceptors(MetadataUtils.newAttachHeadersInterceptor(Metadata().apply {
            put(MetadataKeys.AUTHORIZATION, "secret")
        }))
    }

    // Création de la requête.
    val request = greetRequest {
        name = "Kotlin"
    }

    // Envoi de la requête.
    val response = client.greet(request).await()

    // Affichage de la réponse
    println(response.result)
}