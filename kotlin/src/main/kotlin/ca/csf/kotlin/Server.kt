package ca.csf.kotlin

import ca.csf.hello.GreeterGrpcKt
import ca.csf.hello.Hello.GreetRequest
import ca.csf.hello.Hello.GreetResponse
import ca.csf.hello.greetResponse
import io.grpc.*
import io.grpc.Metadata
import io.grpc.protobuf.services.ProtoReflectionServiceV1

// Implémentation du service Greeter.
class GreeterService : GreeterGrpcKt.GreeterCoroutineImplBase() {

    // Ceci est la fonction gérant l'appel GRPC du service. Remarquez que ce sont les mêmes
    // paramètres que dans le fichier `.proto`.
    override suspend fun greet(request: GreetRequest): GreetResponse {
        // Validation de la requête.
        val name = request.name.trim()
        if (name.isEmpty()) {
            // Retour d'une erreur avec un code 3 (INVALID_ARGUMENT). Pour la liste complète des
            // codes de retour, voir ceci : https://grpc.io/docs/guides/status-codes/
            throw StatusException(Status.INVALID_ARGUMENT.withDescription("name cannot be empty"))
        }

        // Création de la réponse.
        val response = greetResponse {
            result = "Hello $name from Kotlin!"
        }

        // Envoi de la réponse.
        return response
    }

}

// Authentification avec un Interceptor.
class AuthInterceptor(
    private val secrets : Array<String>
) : ServerInterceptor {
    // Cette méthode intercepte l'appel à notre service.
    override fun <ReqT, RespT> interceptCall(
        call: ServerCall<ReqT, RespT>,
        headers: Metadata,
        next: ServerCallHandler<ReqT, RespT>
    ): ServerCall.Listener<ReqT> {

        // Tente d'obtenir la métadonnée "authorization" (équivalent d'un header).
        val metadata = headers[MetadataKeys.AUTHORIZATION]

        // Si la métadonnée n'est n'est pas présente, produire une erreur 3 (INVALID_ARGUMENT).
        if (metadata == null)
            throw Status.INVALID_ARGUMENT.withDescription("missing authorization metadata").asException()

        // Faire l'authentification. Nous faisons une recherche dans un tableau, mais il pourrait y avoir une base de données.
        if (!secrets.contains(metadata))
            throw Status.UNAUTHENTICATED.withDescription("invalid authorization metadata").asException()

        // Sinon, continuer (chain of responsibility).
        return next.startCall(call, headers)
    }
}

// Point de départ du programme.
fun main(args: Array<String>) {
    // Configuration du serveur.
    val builder = ServerBuilder
        .forPort(5000)
        .addService(GreeterService())

    // Ajout d'authentification (si demandé).
    if (args.contains("--auth")) {
        println("Authentification enabled")
        builder.intercept(AuthInterceptor(arrayOf("secret")))
    }

    // Ajout du service de reflection gRPC (seulement si on en est mode développement).
    if (System.getenv("dev") != null) {
        builder.addService(ProtoReflectionServiceV1.newInstance())
    }

    // Création du serveur.
    val server = builder.build()

    // Ceci s'assure de fermer le serveur en même temps que la JVM.
    Runtime.getRuntime().addShutdownHook(Thread {
        server.shutdown().awaitTermination()
    })

    // Démarrage du serveur.
    println("Server is running")
    server.start().awaitTermination()
}
