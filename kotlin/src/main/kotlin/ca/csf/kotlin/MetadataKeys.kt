package ca.csf.kotlin

import io.grpc.Metadata

object MetadataKeys {
    val AUTHORIZATION: Metadata.Key<String> = Metadata.Key.of("authorization", Metadata.ASCII_STRING_MARSHALLER)
}