using Grpc.Core;
using Hello;

namespace CSharp.Services;

// Implémentation du service Greeter.
public class GreeterService : Greeter.GreeterBase
{
    // Microsoft recommande d'ajouter un ILogger et de le prendre à la construction.
    // Nous n'en aurons pas besoin. Sachez qu'il est injecté automatiquement par l'application.
    private readonly ILogger<GreeterService> logger;

    // Constructeur.
    public GreeterService(ILogger<GreeterService> logger)
    {
        this.logger = logger;
    }

    // Ceci est la fonction gérant l'appel GRPC du service. Remarquez que ce sont les mêmes
    // paramètres que dans le fichier `.proto`.
    public override async Task<GreetResponse> Greet(GreetRequest request, ServerCallContext context)
    {
        // Validation de la requête.
        var name = request.Name.Trim();
        if (string.IsNullOrWhiteSpace(name))
        {
            // Retour d'une erreur avec un code 3 (INVALID_ARGUMENT). Pour la liste complète des
            // codes de retour, voir ceci : https://grpc.io/docs/guides/status-codes/
            throw new RpcException(new Status(StatusCode.InvalidArgument, "name cannot be empty"));
        }

        // Création de la réponse.
        var response = new GreetResponse()
        {
            Result = $"Hello {name} from C#!"
        };
        
        // Envoi de la réponse.
        return response;
    }
}