using CSharp.Interceptors;
using CSharp.Services;

// WebApplication permet de configurer une application Web.
var builder = WebApplication.CreateBuilder(args);

// Pour commencer, il faut indiquer à cette application d'accepter les connexions gRPC.
var grpc = builder.Services.AddGrpc(options =>
{
    // Ajout d'authentification sur le service Greeter (si demandé).
    if (args.Contains("--auth"))
    {
        // Il est possible de placer un interceptor devant tous les services. Pour cet exemple, nous allons plutôt le
        // placer dans le GreeterService (voir plus bas), mais voici comment faire quand même.
        //
        //    options.Interceptors.Add<AuthInterceptor>();
        //
    }
});

// Ajout d'authentification sur le service Greeter (si demandé).
grpc.AddServiceOptions<GreeterService>(options =>
{
    if (args.Contains("--auth"))
    {
        Console.WriteLine("Authentification enabled");
        options.Interceptors.Add<AuthInterceptor>([(string[])["secret"]]);
    }
});

// Ajout du service de reflection gRPC (seulement si on en est mode débogage).
#if DEBUG
builder.Services.AddGrpcReflection();
#endif

// Instanciation du serveur.
var app = builder.Build();

// Activation du protocole gRPC pour le web (avec Http/1 en plus de Http/2).
app.UseGrpcWeb(new GrpcWebOptions { DefaultEnabled = true});

// Ajout d'un GreeterService.
app.MapGrpcService<GreeterService>().EnableGrpcWeb();

// Ajout du service de reflection gRPC (seulement en mode DEBUG).
#if DEBUG
app.MapGrpcReflectionService();
#endif

// Exécution du serveur.
Console.WriteLine("Server is running");
app.Run();