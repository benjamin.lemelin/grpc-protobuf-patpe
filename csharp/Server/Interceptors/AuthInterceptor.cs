﻿using Grpc.Core;
using Grpc.Core.Interceptors;

namespace CSharp.Interceptors;

// Authentification avec un Interceptor.
public class AuthInterceptor : Interceptor
{
    private readonly string[] secrets;

    // Constructeur.
    public AuthInterceptor(string[] secrets)
    {
        this.secrets = secrets;
    }

    // Cette méthode intercepte l'appel à notre service.
    public override Task<TResponse> UnaryServerHandler<TRequest, TResponse>
    (
        TRequest request,
        ServerCallContext context,
        UnaryServerMethod<TRequest, TResponse> continuation
    )
    {
        // Tente d'obtenir la métadonnée "authorization" (équivalent d'un header).
        var metadata = context.RequestHeaders.Get("authorization");

        // Si la métadonnée n'est n'est pas présente, produire une erreur 3 (INVALID_ARGUMENT).
        if (metadata is null)
            throw new RpcException(new Status(StatusCode.InvalidArgument, "missing authorization metadata"));

        // Les métadonnées en gRPC peuvent être soit une chaine de caractères ASCII, soit un tableau de bytes.
        // Accepter seulement une chaine ASCII ou produire une erreur 3 (INVALID_ARGUMENT).
        if (metadata.IsBinary)
            throw new RpcException(new Status(StatusCode.InvalidArgument, "malformed authorization metadata"));

        // Faire l'authentification. Nous faisons une recherche dans un tableau, mais il pourrait y avoir une
        // base de données. Si l'authentification échoue, produire une erreur 16 (UNAUTHENTICATED).
        if (!secrets.Contains(metadata.Value))
            throw new RpcException(new Status(StatusCode.Unauthenticated, "invalid authorization metadata"));

        // Sinon, continuer (chain of responsibility).
        return base.UnaryServerHandler(request, context, continuation);
    }
}