![Logo C#](../.docs/csharp-logo.svg)

# C#

Ce projet expérimente avec GRPC et Protobuf en C#.

## Démarrage rapide

Ce projet nécessite d'installer un [Compilateur C#][C# Toolchain] sur votre machine. Veuillez suivre les
instructions en fonction de votre système d'exploitation. Si vous aviez déjà le compilateur C# sur votre machine,
il vous est recommandé de le mettre à jour à sa dernière version avant de débuter.

### Windows

Installez les outils de development C# fournis avec [Visual Studio]. Vérifiez que .NET est à la version 9.0.

### Ubuntu et dérivés

Pour Ubuntu 24.04 et inférieur, inscrivez le dépôt des packages .NET de Microsoft :

```shell
sudo add-apt-repository ppa:dotnet/backports
```

Ensuite, installez le SDK .NET :

```shell
sudo apt install dotnet-sdk-9.0
```

### Fedora

Installez le SDK .NET avec votre gestionnaire de paquets :

```shell
sudo dnf install dotnet-sdk-9.0
```

### MacOs

Téléchargez le paquet d'installation sur [le site de Microsoft](https://learn.microsoft.com/fr-ca/dotnet/core/install/macos) 
et suivez les instructions.

## Exécuter le projet

Utiliser la commande ci-dessous dans un terminal pour démarrer le projet `Server` :

```shell
cd Server
dotnet run main
```

Ensuite, démarrez le projet `Client` pour faire une requête au serveur :

```shell
cd Client
dotnet run main
```

## Exécuter le projet avec authentification

Ajoutez le paramètre `auth` pour activer l'authentification (l'espace entre le `--` et le paramètre est volontaire) :

```shell
cd Server
dotnet run main -- --auth
```

Faites la même chose pour le client (encore une fois, l'espace entre le `--` et le paramètre est volontaire) :

```shell
cd Client
dotnet run main -- --auth
```

## Environnements de développement

Il est recommandé d'utiliser [Rider] en guise d'IDE. Vous pouvez aussi installer [Visual Studio] sur Windows.

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../LICENSE.md) pour les détails.

[C# Toolchain]: https://dotnet.microsoft.com/en-us/download
[Visual Studio]: https://visualstudio.microsoft.com/
[Rider]: https://www.jetbrains.com/rider/
