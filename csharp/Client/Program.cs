﻿using Grpc.Core;
using Hello;
using Grpc.Net.Client;

// Instanciation du client.
using var channel = GrpcChannel.ForAddress("http://127.0.0.1:5000");
var client = new Greeter.GreeterClient(channel);

// Création de la requête.
var request = new GreetRequest { Name = "C#" };
var metadata = new Metadata();

// Ajout d'authentification (si demandé).
if (args.Contains("--auth"))
    metadata.Add("authorization", "secret");

// Envoi de la requête.
try
{
    // Utilisation du client en mode asynchrone.
    var response = await client.GreetAsync(request, metadata);
    
    // Affichage de la réponse (réussite).
    Console.WriteLine(response.Result);
}
catch (RpcException e)
{
    // Affichage du message d'erreur.
    Console.Error.WriteLine($"Error : {e.Status.Detail}");
}