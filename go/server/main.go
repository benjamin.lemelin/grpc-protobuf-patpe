package main

import (
	// Packages de la librairie standard.
	"context"
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
	"net"
	"os"
	"slices"
	"strings"

	// Packages pour gRPC.
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	// Package contenant les définitions des services gRPC définis dans nos fichiers `.proto`.
	pb "gogrpc/proto"
)

// Config
//
// Représente les paramètres du programme.
type Config struct {
	auth bool
}

// parseConfig
//
// Retourne une structure représentant les paramètres du programme.
func parseConfig() Config {
	return Config{
		auth: slices.Contains(os.Args, "--auth"),
	}
}

// GreeterService
//
// Cette structure représentera notre implémentation du service Greeter défini dans le
// fichier `.proto`. Nous allons l'implémenter justement un peu plus bas.
type GreeterService struct {
	pb.UnimplementedGreeterServer
}

// makeGreeterService
//
// Constructeur de GreeterService.
func makeGreeterService() GreeterService {
	return GreeterService{}
}

// Greet
//
// Ceci est la fonction gérant l'appel GRPC du service. Remarquez que ce sont les mêmes
// paramètres que dans le fichier `.proto` (excluant le premier, qui est un objet `Context`).
func (s *GreeterService) Greet(_ context.Context, request *pb.GreetRequest) (*pb.GreetResponse, error) {
	// Validation de la requête.
	name := strings.TrimSpace(request.Name)
	if len(name) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "name cannot be empty")
	}

	// Création de la réponse.
	return &pb.GreetResponse{
		Result: fmt.Sprintf("Hello %s from Go", request.Name),
	}, nil
}

// makeAuthInterceptor
//
// Cette fonction retourne un Interceptor qui s'occupe d'effectuer l'authentification.
func makeAuthInterceptor(secrets []string) grpc.ServerOption {
	return grpc.UnaryInterceptor(func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
		// Récupère les métadonnées (équivalent des meta).
		meta, metaExists := metadata.FromIncomingContext(ctx)

		// Si les métadonnées sont manquantes, produire une erreur 3 (INVALID_ARGUMENT).
		if !metaExists {
			return nil, status.Errorf(codes.InvalidArgument, "missing metadata")
		}

		// Tente d'obtenir la métadonnée "authorization" (équivalent d'un header).
		allAuth, authExists := meta["authorization"]
		if !authExists || len(allAuth) < 1 {
			return nil, status.Errorf(codes.InvalidArgument, "missing authorization metadata")
		}

		// Faire l'authentification. Nous faisons une recherche dans un tableau, mais il pourrait y avoir une
		// base de données. Si l'authentification échoue, produire une erreur 16 (UNAUTHENTICATED).
		auth := allAuth[0]
		if !slices.Contains(secrets, auth) {
			return nil, status.Errorf(codes.Unauthenticated, "missing authorization metadata")
		}

		// À ce stade, l'authentification a réussi. Passer la requête au handler (comme n'importe quel bon middleware).
		return handler(ctx, req)
	})
}

// Point d'entrée du programme.
func main() {
	// Récupération des paramètres.
	config := parseConfig()

	// Configuration du serveur (tel que l'ajout de l'authentification si demandée).
	var options []grpc.ServerOption
	if config.auth {
		log.Println("Authentification enabled")
		options = []grpc.ServerOption{makeAuthInterceptor([]string{"secret"})}
	}

	// Instanciation du server gRPC.
	server := grpc.NewServer(options...)

	// Création du service gRPC.
	greeterService := makeGreeterService()

	// Ajout du service au serveur.
	pb.RegisterGreeterServer(server, &greeterService)

	// Ajout du service de reflection gRPC (seulement si on en est mode développement).
	if len(os.Getenv("dev")) > 0 {
		reflection.Register(server)
	}

	// Écoute sur le port 5000.
	listener, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("failed to listen on port 5000")
	}

	// Démarrage du serveur.
	log.Println("Server is running")
	if err := server.Serve(listener); err != nil {
		log.Fatalf("failed to start server")
	}
}
