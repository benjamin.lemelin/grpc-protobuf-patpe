![Logo Go](../.docs/go-logo.svg)

# Go

Ce projet expérimente avec GRPC et Protobuf en Go.

## Démarrage rapide

Ce projet nécessite d'installer le [Compilateur Go][Go Toolchain] sur votre machine. Veuillez suivre les
instructions en fonction de votre système d'exploitation. Si vous aviez déjà le compilateur Go sur votre machine,
il vous est recommandé de le mettre à jour à sa dernière version avant de débuter.

### Windows

Installez le [Compilateur Go][Go Toolchain] avec le gestionnaire de paquets [WinGet] :

```shell
winget install GoLang.Go
```

### Ubuntu et dérivés

Installez la dernière version du [Compilateur Go][Go Toolchain] avec votre gestionnaire de paquets :

```shell
sudo apt install golang
```

### Fedora

Installez la dernière version du [Compilateur Go][Go Toolchain] avec votre gestionnaire de paquets :

```shell
sudo dnf install golang
```

### MacOs

Utilisez [Homebrew] pour installer la dernière version de [Compilateur Go][Go Toolchain] :

```shell
brew install go
```

## Préparer le projet

Avant toute chose, vous devez installer ces plugins pour le compilateur Protobuf. Il s'agit des générateurs de code pour
le langage Go.

```shell
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
```

Contrairement aux autres langages, vous devez compiler manuellement les fichiers `.proto`. Pour les systèmes sous 
Windows, exécutez les commandes ci-dessous :

```shell
$env:Path = "$env:Path;$(go env GOPATH)\bin"
protoc --go_out=./proto --go-grpc_out=./proto `
       --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative `
       --proto_path=../.proto ../.proto/hello.proto
```

Pour les systèmes sous Linux, utilisez plutôt les commandes suivantes :

```shell
export PATH="$PATH:$(go env GOPATH)/bin"
protoc --go_out=./proto --go-grpc_out=./proto \
       --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative \
       --proto_path=../.proto ../.proto/hello.proto
```

## Exécuter le projet

Exécutez cette commande à l'intérieur du dossier du projet pour démarrer le serveur :

```shell
go run server/main.go
```

Dans un nouveau terminal, exécutez cette commande ci-dessous pour démarrer un client qui fera une requête au serveur :

```shell
go run client/main.go
```

## Exécuter le projet avec authentification

Ajoutez le paramètre `--auth` pour activer l'authentification :

```shell
go run server/main.go -- --auth
```

Faites la même chose pour le client :

```shell
go run client/main.go -- --auth
```

## Environnements de développement

Il est recommandé d'utiliser [GoLand] en guise d'IDE. Il existe aussi [Visual Studio Code] avec 
l'extension [Go][Go VsCode Extension] (qui très populaire et maintenue par Google).

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../LICENSE.md) pour les détails.

[Go Toolchain]: https://go.dev/doc/install
[WinGet]: https://github.com/microsoft/winget-cli
[GoLand]: https://www.jetbrains.com/go/
[Go VsCode Extension]: https://marketplace.visualstudio.com/items?itemName=golang.go
