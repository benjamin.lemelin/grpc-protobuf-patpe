package main

import (
	// Packages de la librairie standard.
	"context"
	"fmt"
	"google.golang.org/grpc/metadata"
	"log"
	"os"
	"slices"
	// Packages pour gRPC.
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	// Package contenant les définitions des services gRPC définis dans nos fichiers `.proto`.
	pb "gogrpc/proto"
)

// Config
//
// Représente les paramètres du programme.
type Config struct {
	auth bool
}

// parseConfig
//
// Retourne une structure représentant les paramètres du programme.
func parseConfig() Config {
	return Config{
		auth: slices.Contains(os.Args, "--auth"),
	}
}

// Point d'entrée du programme.
func main() {
	// Récupération des paramètres.
	config := parseConfig()

	// Instanciation d'une connexion gRPC.
	connection, err := grpc.NewClient("localhost:5000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("failed to create connection")
	}
	defer func() {
		if err := connection.Close(); err != nil {
			log.Fatalf("failed to close connection")
		}
	}()

	// Création du client gRPC.
	greeterClient := pb.NewGreeterClient(connection)

	// Création de la requête.
	request := pb.GreetRequest{Name: "Go"}

	// Ajout d'authentification (si demandé).
	meta := make(metadata.MD)
	if config.auth {
		meta.Append("authorization", "secret")
	}

	// Envoi de la requête.
	ctx := metadata.NewOutgoingContext(context.Background(), meta)
	response, err := greeterClient.Greet(ctx, &request)

	// Affichage de la réponse.
	if err == nil {
		fmt.Println(response.Result)
	} else {
		fmt.Printf("error : %v\n", err)
	}
}
