use crate::database::{Transaction, DatabaseQueryResultExt, DatabaseResultExt};

#[derive(Debug)]
pub struct ProductRepository {
    _phantom : () // Prevents instanciation
}

impl ProductRepository {
    pub async fn find_all(
        db: &mut Transaction<'_>
    ) -> Result<Vec<ProductEntity>, ProductRepositoryError> {
        let query = sqlx::query_as!(
            ProductEntity,
            "SELECT \
                 sku,
                 name,
                 description,
                 price,
                 stock as `stock:u64`
             FROM
                 product
             ORDER BY
                 name
            "
        );

        let products = query
            .fetch_all(db.as_mut())
            .await?;

        Ok(products)
    }

    pub async fn find_by_sku(
        db: &mut Transaction<'_>,
        sku: &str,
    ) -> Result<ProductEntity, ProductRepositoryError> {
        let query = sqlx::query_as!(
            ProductEntity,
            "SELECT \
                 sku,
                 name,
                 description,
                 price,
                 stock as `stock:u64`
             FROM
                 product
             WHERE
                 sku = ?
            ",
            sku
        );

        let product = query.fetch_one(db.as_mut())
            .await
            .map_not_found(|| ProductRepositoryError::NotFound { sku: sku.to_owned() })?;

        Ok(product)
    }

    pub async fn update_stock(
        db: &mut Transaction<'_>,
        sku: &str,
        stock: u64,
    ) -> Result<(), ProductRepositoryError> {
        let stock = stock as i64;
        let query = sqlx::query!(
            "UPDATE \
                 product
             SET
                 stock = ?
             WHERE
                 sku = ?
            ",
            stock,
            sku
        );

        query.execute(db.as_mut())
            .await?
            .map_empty(|| ProductRepositoryError::NotFound { sku: sku.to_owned() })?;

        Ok(())
    }
}

#[derive(Debug)]
pub struct ProductEntity {
    pub sku: String,
    pub name: String,
    pub description: String,
    pub price: f64,
    pub stock: u64,
}

#[derive(Debug, thiserror::Error)]
pub enum ProductRepositoryError {
    #[error("product {sku} not found")]
    NotFound { sku: String },
    #[error("error using the database : {0}")]
    Database(#[from] sqlx::Error),
}