use tonic::{Request, Response, Status};
use tracing::{error, info};

use super::{validate, validate_rule, normalize, normalize_rule};
use super::util::Extract;
use crate::database::{Database, Transaction};
use crate::repository::{ProductEntity, ProductRepository, ProductRepositoryError};

use proto::{
    product_service_server::{ProductService as ProductServiceBase, ProductServiceServer},
    ProductFindAllRequest,
    ProductFindAllResponse,
    ProductFindBySkuRequest,
    ProductFindBySkuResponse,
    ProductReleaseStockRequest,
    ProductReleaseStockResponse,
    ProductReserveStockRequest,
    ProductReserveStockResponse,
    ProductResponse,
};

#[derive(Debug)]
pub struct ProductService {
    db: Database,
}

impl ProductService {
    #[cfg(debug_assertions)]
    pub const FILE_DESCRIPTOR_SET: &'static [u8] = proto::FILE_DESCRIPTOR_SET;

    pub fn new(db: Database) -> ProductServiceServer<Self> {
        ProductServiceServer::new(Self {
            db
        })
    }
}

#[tonic::async_trait]
impl ProductServiceBase for ProductService {
    async fn find_all(
        &self,
        _: Request<ProductFindAllRequest>,
    ) -> Result<Response<ProductFindAllResponse>, Status> {
        let mut transaction = Transaction::new(&self.db).await?;
        let products = ProductRepository::find_all(&mut transaction).await?;

        Ok(Response::new(ProductFindAllResponse {
            products: products.into_iter().map(From::from).collect(),
        }))
    }

    async fn find_by_sku(
        &self,
        mut request: Request<ProductFindBySkuRequest>,
    ) -> Result<Response<ProductFindBySkuResponse>, Status> {
        let request = request.extract()?;

        let mut transaction = Transaction::new(&self.db).await?;
        let product = ProductRepository::find_by_sku(&mut transaction, &request.sku).await?;

        Ok(Response::new(ProductFindBySkuResponse {
            product: product.into()
        }))
    }

    async fn reserve_stock(
        &self,
        mut request: Request<ProductReserveStockRequest>,
    ) -> Result<Response<ProductReserveStockResponse>, Status> {
        let request = request.extract_mut()?;

        let mut transaction = Transaction::new(&self.db).await?;
        let mut product = ProductRepository::find_by_sku(&mut transaction, &request.sku).await?;

        if product.stock >= request.quantity {
            product.stock -= request.quantity;
            ProductRepository::update_stock(&mut transaction, &request.sku, product.stock).await?;
            transaction.commit().await?;

            Ok(Response::new(ProductReserveStockResponse {
                stock: product.stock
            }))
        } else {
            Err(Status::failed_precondition("stocks are insufficient"))
        }
    }

    async fn release_stock(
        &self,
        mut request: Request<ProductReleaseStockRequest>,
    ) -> Result<Response<ProductReleaseStockResponse>, Status> {
        let request = request.extract_mut()?;

        let mut transaction = Transaction::new(&self.db).await?;
        let mut product = ProductRepository::find_by_sku(&mut transaction, &request.sku).await?;

        product.stock += request.quantity;
        ProductRepository::update_stock(&mut transaction, &request.sku, product.stock).await?;
        transaction.commit().await?;

        Ok(Response::new(ProductReleaseStockResponse {
            stock: product.stock
        }))
    }
}

validate!(
    ProductFindBySkuRequest,
    (sku, required, "sku must be provided")
);

validate!(
    ProductReserveStockRequest,
    (sku, required, "sku must be provided"),
    (quantity, min:1, "quantity must be greater than 0"),
);

validate!(
    ProductReleaseStockRequest,
    (sku, required, "sku must be provided"),
    (quantity, min:1, "quantity must be greater than 0"),
);

normalize!(
    ProductFindBySkuRequest,
    (sku, trim)
);

normalize!(
    ProductReserveStockRequest,
    (sku, trim)
);

normalize!(
    ProductReleaseStockRequest,
    (sku, trim)
);

impl From<ProductRepositoryError> for Status {
    fn from(value: ProductRepositoryError) -> Self {
        match value {
            ProductRepositoryError::NotFound { ref sku } => {
                info!("{value}");
                Status::not_found(format!("product \"{sku}\" not found"))
            }
            _ => {
                error!("{value}");
                Status::internal("internal server error")
            }
        }
    }
}

impl From<ProductEntity> for ProductResponse {
    fn from(value: ProductEntity) -> Self {
        Self {
            sku: value.sku,
            name: value.name,
            description: value.description,
            price: value.price,
            stock: value.stock,
        }
    }
}

impl From<ProductEntity> for Option<ProductResponse> {
    fn from(value: ProductEntity) -> Self {
        Some(value.into())
    }
}

mod proto {
    tonic::include_proto!("product");

    pub const FILE_DESCRIPTOR_SET: &[u8] = tonic::include_file_descriptor_set!("product_descriptor");
}