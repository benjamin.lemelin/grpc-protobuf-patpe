use tonic::Request;

pub trait Validate {
    type Error;

    fn validate(&self) -> Result<(), Self::Error> {
        Ok(())
    }
}

pub trait Normalize {
    type Error;

    fn normalize(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

pub trait Extract {
    type Request;
    type Error;

    fn extract(&mut self) -> Result<&Self::Request, Self::Error>;
    fn extract_mut(&mut self) -> Result<&mut Self::Request, Self::Error>;
}

impl<T, E> Extract for Request<T>
where
    T: Validate<Error=E> + Normalize<Error=E>
{
    type Request = T;
    type Error = E;

    fn extract(&mut self) -> Result<&Self::Request, Self::Error> {
        Ok(self.extract_mut()?)
    }

    fn extract_mut(&mut self) -> Result<&mut Self::Request, Self::Error> {
        let request = self.get_mut();

        request.validate()?;
        request.normalize()?;

        Ok(request)
    }
}