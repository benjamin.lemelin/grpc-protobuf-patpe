use tonic::Status;
use tracing::{error, info};

use crate::database::DatabaseError;
use crate::repository::ProductRepositoryError;

pub use product::*;

mod product;
mod util;

macro_rules! validate {
    (
        $struct_name:ident,
        $( ($field:ident , $rule:ident $( : $param:expr )? , $message:expr) ),* $(,)?
    ) => {
        impl crate::api::util::Validate for $struct_name {
            type Error = tonic::Status;

            fn validate(&self) -> Result<(), Self::Error> {
                $(
                    if validate_rule!(self.$field, $rule $(, $param)?) {
                        return Err(Status::invalid_argument($message));
                    }
                )*
                Ok(())
            }
        }
    };
}

macro_rules! validate_rule {
    ($field:expr, required) => {
        $field.trim().is_empty()
    };
    ($field:expr, min, $min:expr) => {
        $field < $min
    };
}

pub(self) use {validate, validate_rule};

macro_rules! normalize {
    (
        $struct_name:ident,
        $( ($field:ident , $rule:ident $( : $param:expr )?) ),* $(,)?
    ) => {
        impl crate::api::util::Normalize for $struct_name {
            type Error = tonic::Status;

            fn normalize(&mut self) -> Result<(), Self::Error> {
                $(
                    normalize_rule!(self.$field, $rule $(, $param)?);
                )*
                Ok(())
            }
        }
    };
}

macro_rules! normalize_rule {
    ($field:expr, trim) => {
        $field = $field.trim().to_owned()
    };
}

pub(self) use {normalize, normalize_rule};

impl From<DatabaseError> for Status {
    fn from(value: DatabaseError) -> Self {
        error!("{value}");
        Status::internal("internal server error")
    }
}