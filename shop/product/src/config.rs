use std::net::SocketAddr;
use std::path::PathBuf;
use clap::Parser;

#[derive(Debug, Parser)]
#[command(author, version, about)]
pub struct Config {
    #[arg(long, default_value = "5000")]
    port: u16,
    #[arg(long, default_value = ".shop/")]
    data_dir: PathBuf,
}

impl Config {
    pub fn parse() -> Self {
        Parser::parse()
    }

    pub fn server_address(&self) -> SocketAddr {
        ([0,0,0,0], self.port).into()
    }

    pub fn database_path(&self) -> PathBuf {
        self.data_dir.join("app.db")
    }
}