use std::ops::{Deref, DerefMut};
use std::path::Path;
use std::io;

use sqlx::{sqlite::SqliteConnectOptions, ConnectOptions, SqlitePool, SqliteTransaction};
use tokio::fs;

pub use util::*;

mod util;

#[derive(Debug)]
pub struct Database(SqlitePool);

impl Database {
    pub async fn new(path: impl AsRef<Path>) -> Result<Self, DatabaseError> {
        let path = path.as_ref();

        fs::create_dir_all(path.parent().ok_or_else(|| io::Error::new(
            io::ErrorKind::Other,
            "database path is not inside a directory",
        ))?).await?;

        let options = SqliteConnectOptions::new()
            .filename(path)
            .create_if_missing(true)
            .disable_statement_logging();

        let database = SqlitePool::connect_with(options).await?;
        sqlx::migrate!().run(&database).await.map_err(Into::<sqlx::Error>::into)?;

        Ok(Self(database))
    }
}

#[derive(Debug)]
pub struct Transaction<'c>(SqliteTransaction<'c>);

impl<'c> Transaction<'c> {
    pub async fn new(db: &Database) -> Result<Self, DatabaseError> {
        Ok(Self(db.0.begin().await?))
    }

    pub async fn commit(self) -> Result<(), DatabaseError> {
        Ok(self.0.commit().await?)
    }

    #[allow(unused)]
    pub async fn rollback(self) -> Result<(), DatabaseError> {
        Ok(self.0.rollback().await?)
    }
}

impl<'c> Deref for Transaction<'c> {
    type Target = SqliteTransaction<'c>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'c> DerefMut for Transaction<'c> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Debug, thiserror::Error)]
pub enum DatabaseError {
    #[error("sql error using the database : {0}")]
    Sql(#[from] sqlx::Error),
    #[error("io error using the database : {0}")]
    Io(#[from] io::Error),
}