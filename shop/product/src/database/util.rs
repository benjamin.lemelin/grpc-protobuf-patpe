use sqlx::sqlite::SqliteQueryResult;

pub trait DatabaseResultExt<T> {
    fn map_not_found<E, F>(self, op: F) -> Result<T, E>
    where
        Self: Sized,
        E: From<sqlx::Error>,
        F: FnOnce() -> E;
}

impl<T> DatabaseResultExt<T> for sqlx::Result<T> {
    fn map_not_found<E, F>(self, op: F) -> Result<T, E>
    where
        Self: Sized,
        E: From<sqlx::Error>,
        F: FnOnce() -> E,
    {
        match self {
            Ok(t) => Ok(t),
            Err(sqlx::Error::RowNotFound) => Err(op()),
            Err(e) => Err(e.into())
        }
    }
}

pub trait DatabaseQueryResultExt {
    fn map_empty<E, F>(self, op: F) -> Result<u64, E>
    where
        F: FnOnce() -> E;
}

impl DatabaseQueryResultExt for SqliteQueryResult {
    fn map_empty<E, F>(self, op: F) -> Result<u64, E>
    where
        F: FnOnce() -> E
    {
        match self.rows_affected() {
            0 => Err(op()),
            rows => Ok(rows),
        }
    }
}