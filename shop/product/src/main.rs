#[deny(missing_debug_implementations)]
use std::error::Error;

use tonic::transport::Server;
#[cfg(debug_assertions)]
use tonic_reflection::server::{ServerReflection, ServerReflectionServer};
use tower_http::trace::TraceLayer;

use api::ProductService;
use config::Config;
use crate::database::Database;

mod database;

mod api;
mod config;
mod repository;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenvy::dotenv().ok();
    tracing_subscriber::fmt::init();

    let config = Config::parse();
    let database = Database::new(config.database_path()).await?;
    let product_service = ProductService::new(database);

    let server = Server::builder()
        .layer(TraceLayer::new_for_grpc())
        .add_service(product_service);

    #[cfg(debug_assertions)]
    let server = server.add_service(create_reflexion_service());

    println!("Product service started");
    server.serve(config.server_address()).await?;

    Ok(())
}

#[cfg(debug_assertions)]
fn create_reflexion_service() -> ServerReflectionServer<impl ServerReflection> {
    tonic_reflection::server::Builder::configure()
        .register_encoded_file_descriptor_set(ProductService::FILE_DESCRIPTOR_SET)
        .build_v1()
        .expect("File descriptor set should be valid")
}