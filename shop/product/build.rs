use std::error::Error;
use std::{env, io, path::PathBuf};

fn main() -> Result<(), Box<dyn Error>> {
    configure_database();
    configure_protos()?;

    Ok(())
}

fn configure_database() {
    // Trigger recompilation when a new migration is added. This is to ensure SQLX migrations are
    // always up to date.
    println!("cargo:rerun-if-changed=migrations");
}

fn configure_protos() -> io::Result<()> {
    // Generate Rust code from Protobuf files. Code will be automatically regenerated if Protobuf
    // files are changed.
    let out_dir = PathBuf::from(env::var("OUT_DIR").expect("Variable should be provided by Cargo"));
    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("product_descriptor.bin"))
        .compile_protos(&["../.proto/product.proto"], &["../.proto"])
}
