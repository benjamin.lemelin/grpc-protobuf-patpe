# Product Service

Service gRPC pour les produits (rédigé en Rust).

## Démarrage rapide

Ce projet nécessite d'installer le [Compilateur Rust][Rust] sur votre machine. Veuillez suivre les instructions en 
fonction de votre système d'exploitation. Si vous aviez déjà le compilateur Rust sur votre machine, il vous est 
recommandé de le mettre à jour à sa dernière version avant de débuter.

Le package [sqlx] fournit une vérification syntaxique et sémantique des requêtes SQL à la compilation. Pour y parvenir, 
une base de données active doit être disponible au moment de la compilation. Pour commencer, installez les outils [sqlx]
avec support pour *SQLite* et *rustls* en utilisant cette commande :

```shell
cargo install sqlx-cli --no-default-features --features sqlite,rustls
```

Ensuite, pour créer la base de données, exécutez simplement ces commandes :

```shell
mkdir -p .shop

sqlx database create
sqlx migrate run
```

Une fois tout cela fait, vous pourrez alors exécuter le projet :

```shell
cargo run
```

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../../LICENSE.md) pour les détails.

[Rust]: https://www.rust-lang.org/
[sqlx]: https://github.com/launchbadge/sqlx