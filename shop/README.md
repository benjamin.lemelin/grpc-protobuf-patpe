# Magasin

Prendre le site d'achat en ligne de la baie d'ourson (moins impressionnant que le Grimoire, mais bon).

1. Point d'entrée (Backend et Frontend) (Kotlin) : Appels d'API et pages web (utiliser Ktor). 
2. Service Utilisateur (Java) : Vérifier et créer les comptes utilisateurs.
3. Service Produits (Rust) : Gérer la liste des produits et l'inventaire.
4. Service Commandes (C#) : Effectuer les commandes. Se sert du Service Produits pour vérifier et modifier l'inventaire.
5. Service Paiement (Go) : Simule un paiement. 