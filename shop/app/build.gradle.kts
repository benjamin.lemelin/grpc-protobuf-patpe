val kotlinVersion = "2.1.10"
val ktorVersion = "3.1.1"
val kotlinxVersion = "1.9.0"

val protobufVersion = "4.29.1"
val grpcVersion = "1.68.2"
val grpcNettyVersion = "1.68.2"
val grpcKotlinVersion = "1.4.1"

val dotenvKotlinVersion = "6.5.1"
val logbackVersion = "1.4.14"

plugins {
    kotlin("jvm") version "2.1.10"
    id("io.ktor.plugin") version "3.1.1"
    id("org.jetbrains.kotlin.plugin.serialization") version "2.1.10"
    id("com.google.protobuf") version "0.9.4"
}

group = "ca.csf.shop"
version = "1.0.0"

application {
    mainClass = "ca.csf.shop.ApplicationKt"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    implementation("io.ktor:ktor-server-sessions:$ktorVersion")
    implementation("io.ktor:ktor-server-host-common:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-server-thymeleaf:$ktorVersion")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-guava:$kotlinxVersion")
    implementation("com.google.protobuf:protobuf-kotlin:$protobufVersion")
    implementation("io.grpc:grpc-protobuf:$grpcVersion")
    implementation("io.grpc:grpc-services:$grpcVersion")
    implementation("io.grpc:grpc-netty:$grpcNettyVersion")
    implementation("io.grpc:grpc-kotlin-stub:$grpcKotlinVersion")

    implementation("io.github.cdimascio:dotenv-kotlin:$dotenvKotlinVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
}

sourceSets {
    main {
        proto {
            srcDir("../.proto")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
    plugins {
        create("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
        create("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpcKotlinVersion:jdk8@jar"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                create("grpc")
                create("grpckt")
            }
            it.builtins {
                create("kotlin")
            }
        }
    }
}
