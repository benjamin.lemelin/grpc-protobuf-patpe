/**
 * Implemented by objects that can be validated.
 */
interface Validable {
    /**
     * Validates the object, returning a list of all validation errors (if any). Otherwise, returns an empty list.
     */
    fun validate() : MutableList<String>
}