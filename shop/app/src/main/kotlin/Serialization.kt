import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.serialization.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.routing.*
import io.ktor.util.reflect.*
import io.ktor.utils.io.*
import io.ktor.utils.io.charsets.*
import util.toCamelCase
import java.net.URLDecoder
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

fun Application.configureSerialization() {
    install(ContentNegotiation) {
        register(ContentType.Application.FormUrlEncoded, UrlEncodedFormDecoder())
    }
}

// Ktor does not have an url encoded form decoder for requests. This is a quick and dirty one that allows us to
// deserialize to an object.
class UrlEncodedFormDecoder : ContentConverter {
    override suspend fun serialize(
        contentType: ContentType,
        charset: Charset,
        typeInfo: TypeInfo,
        value: Any?
    ): OutgoingContent? {
        throw NotImplementedError("Not be implemented")
    }

    override suspend fun deserialize(charset: Charset, typeInfo: TypeInfo, content: ByteReadChannel): Any? {
        val constructor = typeInfo.type.primaryConstructor ?: return null
        val args = mutableMapOf<KParameter, Any>()

        for (record in content.readRemaining().readText().split("&")) {
            val parts = record.split("=")
            if (parts.size != 2) return null

            val name = parts[0].toCamelCase()
            val value = URLDecoder.decode(parts[1], charset)

            val parameter = constructor.parameters.firstOrNull { it.name == name } ?: return null
            args[parameter] = value
        }

        return try {
            constructor.callBy(args)
        } catch (e: Exception) {
            null
        }
    }
}