package user

import ca.csf.shop.user.UserServiceGrpc
import ca.csf.shop.user.UserServiceGrpc.UserServiceFutureStub
import ca.csf.shop.user.userCreateRequest
import ca.csf.shop.user.userLoginRequest
import io.grpc.ManagedChannelBuilder
import io.grpc.Status
import io.grpc.StatusRuntimeException
import kotlinx.coroutines.guava.await

class UserService(address: String) {
    private val client: UserServiceFutureStub = UserServiceGrpc.newFutureStub(
        ManagedChannelBuilder.forTarget(address).usePlaintext().build()
    )

    suspend fun create(email: String, password: String) : Boolean {
        try {
            client
                .create(userCreateRequest {
                    this.email = email
                    this.password = password
                })
                .await()
            return true
        } catch (e : StatusRuntimeException) {
            when (e.status.code) {
                Status.FAILED_PRECONDITION.code -> return false
                else -> throw e
            }
        }
    }

    suspend fun login(email: String, password: String) : Boolean {
        try {
            client
                .login(userLoginRequest {
                    this.email = email
                    this.password = password
                })
                .await()
            return true
        } catch (e : StatusRuntimeException) {
            when (e.status.code) {
                Status.NOT_FOUND.code -> return false
                else -> throw e
            }
        }
    }
}