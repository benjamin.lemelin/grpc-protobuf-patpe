package user

import Validable
import kotlinx.serialization.Serializable
import util.isEmail

@Serializable
data class UserSignInRequest(
    var email: String = "",
    var password: String = ""
) : Validable {

    init {
        email = email.trim()
    }

    override fun validate(): MutableList<String> {
        val errors = mutableListOf<String>()

        if (email.isEmpty())
            errors.add("L'adresse de courriel est obligatoire.")
        else if (!email.isEmail())
            errors.add("L'adresse de courriel est invalide.")

        if (password.isEmpty())
            errors.add("Le mot de passe est obligatoire.")

        return errors
    }
}