import io.ktor.server.application.*
import io.ktor.server.sessions.*
import kotlinx.serialization.Serializable
import user.User
import util.orDefault

fun Application.configureSession() {
    install(Sessions) {
        cookie<Session>("SHOP_SESSION", SessionStorageMemory()) {
            cookie.extensions["SameSite"] = "lax"
        }
    }
}

@Serializable
data class Session(
    var user: User? = null,
)

/**
 * Gets a session instance or instantiate a new one using the default primary constructor of the type [T].
 *
 * @throws IllegalArgumentException if no default primary constructor exists for the type [T].
 */
inline fun <reified T : Any> CurrentSession.getOrDefault(): T {
    return get<T>().orDefault()
}