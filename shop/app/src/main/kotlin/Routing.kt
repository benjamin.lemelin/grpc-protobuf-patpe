import io.github.cdimascio.dotenv.dotenv
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import io.ktor.server.util.*
import product.ProductService
import user.User
import user.UserService
import user.UserSignInRequest
import user.UserSignUpRequest
import util.body
import util.respond

fun Application.configureRouting() {
    val env = dotenv()
    val productService = ProductService(env["PRODUCT_SERVER_ADDRESS"])
    val userService = UserService(env["USER_SERVER_ADDRESS"])

    routing {
        get("/") {
            val session = call.sessions.getOrDefault<Session>()
            val products = productService.findAll()

            call.respond()
                .withSession(session)
                .withTemplate(
                    "index", mapOf(
                        "session" to session,
                        "products" to products
                    )
                )
        }

        get("/product/{sku}") {
            val sku = call.parameters.getOrFail("sku")
            val session = call.sessions.getOrDefault<Session>()
            val product = productService.findBySku(sku)

            call.respond()
                .withSession(session)
                .withTemplate(
                    "product", mapOf(
                        "session" to session,
                        "product" to product
                    )
                )
        }

        get("/sign-up") {
            val session = call.sessions.getOrDefault<Session>()
            if (session.user != null) return@get call.respondRedirect("/")

            val request = UserSignUpRequest()

            call.respond()
                .withSession(session)
                .withTemplate(
                    "sign-up", mapOf(
                        "form" to request
                    )
                )
        }

        post("/sign-up") {
            val session = call.sessions.getOrDefault<Session>()
            if (session.user != null) return@post call.respondRedirect("/")

            val request = call.body.getOrDefault<UserSignUpRequest>()
            val errors = request.validate()

            if (errors.isEmpty()) {
                if (!userService.create(request.email, request.password)) {
                    errors.add("Cette adresse de courriel est déjà utilisée.")
                }
            }

            if (errors.isEmpty()) {
                call.respond()
                    .withSession(session)
                    .withRedirect("/")
            } else {
                call.respond()
                    .withSession(session)
                    .withTemplate(
                        "sign-up", mapOf(
                            "form" to request,
                            "errors" to errors
                        )
                    )
            }
        }

        get("/sign-in") {
            val session = call.sessions.getOrDefault<Session>()
            if (session.user != null) return@get call.respondRedirect("/")

            val request = UserSignInRequest()

            call.respond()
                .withSession(session)
                .withTemplate(
                    "sign-in", mapOf(
                        "form" to request
                    )
                )
        }

        post("/sign-in") {
            val session = call.sessions.getOrDefault<Session>()
            if (session.user != null) return@post call.respondRedirect("/")

            val request = call.body.getOrDefault<UserSignInRequest>()
            val errors = request.validate()

            if (errors.isEmpty()) {
                if (userService.login(request.email, request.password)) {
                    session.user = User(request.email)
                } else {
                    errors.add("Combinaison incorrecte de nom d'utilisateur et de mot de passe.")
                }
            }

            if (errors.isEmpty()) {
                call.respond()
                    .withSession(session)
                    .withRedirect("/")
            } else {
                call.respond()
                    .withSession(session)
                    .withTemplate(
                        "sign-in", mapOf(
                            "form" to request,
                            "errors" to errors
                        )
                    )
            }
        }

        get("/sign-out") {
            val session = call.sessions.getOrDefault<Session>()
            session.user = null

            call.respond()
                .withSession(session)
                .withRedirect("/")
        }

        staticResources("/assets", "static")
    }

    install(StatusPages) {
        exception<Throwable> { call, cause ->
            call.respondText(text = "500: $cause", status = HttpStatusCode.InternalServerError)
        }
    }
}
