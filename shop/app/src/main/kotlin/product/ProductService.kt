package product

import ca.csf.shop.product.ProductServiceGrpc
import ca.csf.shop.product.ProductServiceGrpc.ProductServiceFutureStub
import ca.csf.shop.product.productFindAllRequest
import ca.csf.shop.product.productFindBySkuRequest
import io.grpc.ManagedChannelBuilder
import kotlinx.coroutines.guava.await

class ProductService(address: String) {
    private val client: ProductServiceFutureStub = ProductServiceGrpc.newFutureStub(
        ManagedChannelBuilder.forTarget(address).usePlaintext().build()
    )

    suspend fun findAll(): List<Product> {
        return client
            .findAll(productFindAllRequest { })
            .await()
            .productsList
    }

    suspend fun findBySku(sku: String): Product {
        return client
            .findBySku(productFindBySkuRequest { this.sku = sku })
            .await()
            .product
    }
}