package util

import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

/**
 * Instantiate a new instance using the default primary constructor of the type [T].
 *
 * @throws IllegalArgumentException if no default primary constructor exists for the type [T].
 */
inline fun <reified T : Any> KClass<T>.default(): T {
    val primaryConstructor = primaryConstructor
        ?: throw IllegalArgumentException("Class ${T::class.simpleName} does not have a primary constructor.")

    if (primaryConstructor.parameters.any { !it.isOptional })
        throw IllegalArgumentException("Class ${T::class.simpleName} does not have a default constructor.")

    return primaryConstructor.callBy(emptyMap())
}

/**
 * Gets or instantiate a new instance using the default primary constructor of the type [T].
 *
 * @throws IllegalArgumentException if no default primary constructor exists for the type [T].
 */
inline fun <reified T : Any> T?.orDefault(): T {
    return this ?: T::class.default()
}