package util

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import io.ktor.server.thymeleaf.*

/**
 * Create a response builder.
 */
fun ApplicationCall.respond(): ResponseBuilder {
    return ResponseBuilder(this)
}

@JvmInline
value class ResponseBuilder(val call: ApplicationCall) {
    /**
     * Save object within session.
     */
    inline fun <reified T> withSession(value: T): ResponseBuilder {
        call.sessions.set(value)
        return this
    }

    /**
     * Respond with a [template] applying a data [model].
     */
    suspend fun withTemplate(template : String, model : Map<String, Any> = emptyMap()) {
        call.respondTemplate(template, model)
    }

    /**
     * Responds with a redirection.
     */
    suspend fun withRedirect(url : String) {
        call.respondRedirect(url)
    }
}