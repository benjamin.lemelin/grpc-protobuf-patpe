package util

import io.ktor.server.plugins.ContentTransformationException
import io.ktor.server.request.*
import io.ktor.server.routing.*

/**
 * Gets the request body.
 */
val RoutingCall.body: RequestExtractor
    get() = RequestExtractor(this)

@JvmInline
value class RequestExtractor(val call: RoutingCall) {
    /**
     * Receives content from request, or return a default value if none was provided or if the format is invalid.
     *
     * @return instance of [T] received from request, or a default one.
     */
    suspend inline fun <reified T : Any> getOrDefault(): T {
        return try {
            this.call.receive<T>()
        } catch (e : ContentTransformationException) {
            T::class.default()
        }
    }
}