package util

/**
 * Email validation regex.
 */
private val EMAIL_REGEX = Regex("[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,}")

/**
 * Transform string content to camel case, returning a new one.
 */
fun String.toCamelCase(): String {
    return this.split("-")
        .mapIndexed { i, it -> if (i == 0) it else it.replaceFirstChar { char -> char.uppercase() } }
        .joinToString("")
}

/**
 * Returns true if the strings contains a valid email.
 */
fun String.isEmail(): Boolean {
    return EMAIL_REGEX.matches(this)
}