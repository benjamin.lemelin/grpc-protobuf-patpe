﻿using Microsoft.Data.Sqlite;

namespace Order.Repository;

public sealed class OrderRepository : IDisposable, IAsyncDisposable
{
    private readonly Database.Database db;

    public OrderRepository(Database.Database db)
    {
        this.db = db;
    }
    
    public void InsertOne(OrderEntity order)
    {
        using var createOrderCommand = db.CreateCommand();
        createOrderCommand.CommandText =
            """
            INSERT INTO `order`
                (email)
            VALUES
                (@email);

            SELECT last_insert_rowid();
            """;
        createOrderCommand.Parameters.Add(new SqliteParameter("@email", order.Email));
        order.Id = createOrderCommand.ExecuteScalar() as int?;

        foreach (var item in order.Items)
        {
            using var createItemCommand = db.CreateCommand();
            createOrderCommand.CommandText =
                """
                INSERT INTO `order_item`
                    (order_id, sku, quantity)
                VALUES
                    (@order_id, @sku, @quantity);

                SELECT last_insert_rowid();
                """;
            createItemCommand.Parameters.Add(new SqliteParameter("@order_id", order.Id));
            createItemCommand.Parameters.Add(new SqliteParameter("@sku", item.Sku));
            createItemCommand.Parameters.Add(new SqliteParameter("@quantity", item.Quantity));
            item.Id = createItemCommand.ExecuteScalar() as int?;
        }
    }

    public void Dispose()
    {
        db.Dispose();
    }

    public async ValueTask DisposeAsync()
    {
        await db.DisposeAsync();
    }
}