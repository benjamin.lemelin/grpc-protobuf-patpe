﻿namespace Order.Repository;

public record OrderEntity
{
    public int? Id { get; set; }
    public required string Email { get; set; }
    public required List<OrderItemEntity> Items { get; init; }
}

public record OrderItemEntity
{
    public int? Id { get; set; }
    public required string Sku { get; set; }
    public required int Quantity { get; set; }
}