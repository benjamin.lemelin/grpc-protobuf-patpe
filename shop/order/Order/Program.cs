﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Order.Services;

var builder = WebApplication.CreateBuilder(args);
var grpc = builder.Services.AddGrpc();
#if DEBUG
grpc.Services.AddGrpcReflection();
#endif

var app = builder.Build();
app.MapGrpcService<OrderService>();

Console.WriteLine("Server is running");
app.Run();