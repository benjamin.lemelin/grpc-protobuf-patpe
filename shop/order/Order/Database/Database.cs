﻿using System.Data.Common;
using Microsoft.Data.Sqlite;

namespace Order.Database;

public sealed class Database : IDisposable, IAsyncDisposable
{
    private readonly SqliteConnection connection;
    
    public Database(string path)
    {
        connection = new SqliteConnection($"Data Source={path}");
        connection.Open();
        
        InitializeDatabase();
    }
    
    private void InitializeDatabase()
    {
        using var command = connection.CreateCommand();
        command.CommandText =
            """
            CREATE TABLE IF NOT EXISTS `order`
            (
                id      INTEGER     NOT NULL PRIMARY KEY AUTOINCREMENT,
                email   TEXT        NOT NULL
            );
            CREATE TABLE IF NOT EXISTS `order_item`
            (
                id          INTEGER     NOT NULL PRIMARY KEY AUTOINCREMENT,
                order_id    INTEGER     NOT NULL REFERENCES `order` (id) ON DELETE CASCADE,
                sku         TEXT        NOT NULL,
                quantity    INTEGER     NOT NULL
            );
            """;
        command.ExecuteNonQuery();
    }

    public SqliteTransaction BeginTransaction()
    {
        return connection.BeginTransaction();
    }

    public SqliteCommand CreateCommand()
    {
        return connection.CreateCommand();
    }

    public void Dispose()
    {
        connection.Dispose();
    }

    public async ValueTask DisposeAsync()
    {
        await connection.DisposeAsync();
    }
}