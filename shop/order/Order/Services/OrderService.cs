﻿using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Order.Services;

public class OrderService : Order.OrderService.OrderServiceBase
{
    private readonly ILogger<OrderService> logger;

    public OrderService(ILogger<OrderService> logger)
    {
        this.logger = logger;
    }

    public override async Task<OrderCreateResponse> Create(OrderCreateRequest request, ServerCallContext context)
    {
        return null;
    }
}