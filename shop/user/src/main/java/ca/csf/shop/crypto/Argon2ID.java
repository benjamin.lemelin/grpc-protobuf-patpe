package ca.csf.shop.crypto;

import org.bouncycastle.crypto.generators.Argon2BytesGenerator;
import org.bouncycastle.crypto.params.Argon2Parameters;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.regex.Pattern;

public final class Argon2ID {
    private static final int VERSION = Argon2Parameters.ARGON2_id;
    private static final int SALT_LENGTH = 16; // 16 bytes salt.
    private static final int HASH_LENGTH = 32; // 32 bytes hash.
    private static final int MEMORY = 19_456;  // 19 mebibyte of memory.
    private static final int PARALLELISM = 1;  // 1 thread.
    private static final int ITERATIONS = 2;   // 2 iterations.

    private static final String PHC = "$argon2id$v=%d$m=%d,p=%d,t=%d,$%s$%s";
    private static final Pattern PHC_REGEX = Pattern.compile("\\$argon2id\\$v=(\\d+)\\$m=(\\d+),p=(\\d+),t=(\\d+),\\$(.+)\\$(.+)");

    private final SecureRandom random;

    public Argon2ID() {
        random = new SecureRandom();
    }

    public String encode(String password) {
        var saltBytes = new byte[SALT_LENGTH];
        random.nextBytes(saltBytes);

        var argon2 = new Argon2BytesGenerator();
        argon2.init(new Argon2Parameters
            .Builder(VERSION)
            .withMemoryAsKB(MEMORY)
            .withParallelism(PARALLELISM)
            .withIterations(ITERATIONS)
            .withSalt(saltBytes)
            .build()
        );

        var hashBytes = new byte[HASH_LENGTH];
        argon2.generateBytes(password.getBytes(StandardCharsets.UTF_8), hashBytes);

        var base64Encoder = Base64.getEncoder();
        var saltBase64 = base64Encoder.encodeToString(saltBytes);
        var hashBase64 = base64Encoder.encodeToString(hashBytes);

        return String.format(
            PHC, VERSION, MEMORY, PARALLELISM, ITERATIONS, saltBase64, hashBase64
        );
    }

    public boolean verify(String password, String storedHash) {
        var matcher = PHC_REGEX.matcher(storedHash);
        if (!matcher.matches())
            throw new IllegalArgumentException("Input hash is not a valid PHC string with Argon2ID.");

        var base64Decoder = Base64.getDecoder();
        var version = Integer.parseInt(matcher.group(1));
        var memory = Integer.parseInt(matcher.group(2));
        var parallelism = Integer.parseInt(matcher.group(3));
        var iterations = Integer.parseInt(matcher.group(4));
        var saltBytes = base64Decoder.decode(matcher.group(5));
        var hashBytes = base64Decoder.decode(matcher.group(6));

        var argon2 = new Argon2BytesGenerator();
        argon2.init(new Argon2Parameters
            .Builder(version)
            .withMemoryAsKB(memory)
            .withParallelism(parallelism)
            .withIterations(iterations)
            .withSalt(saltBytes)
            .build()
        );

        byte[] computedHashBytes = new byte[hashBytes.length];
        argon2.generateBytes(password.getBytes(StandardCharsets.UTF_8), computedHashBytes);

        return MessageDigest.isEqual(hashBytes, computedHashBytes);
    }
}
