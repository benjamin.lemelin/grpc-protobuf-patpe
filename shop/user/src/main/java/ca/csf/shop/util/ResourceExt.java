package ca.csf.shop.util;

import java.nio.file.Files;
import java.nio.file.Paths;

import static ca.csf.shop.util.ExceptionExt.expect;

/**
 * Utilitaire pour la gestion des ressources.
 */
public final class ResourceExt {

    /**
     * Constructeur privé pour empêcher l'instanciation de cette classe utilitaire.
     */
    private ResourceExt() {
        // Privé pour empêcher l'instanciation.
    }

    /**
     * Lit le contenu d'une ressource en tant que chaîne de caractères.
     *
     * @param path Le chemin de la ressource à lire.
     * @return Le contenu de la ressource sous forme de chaîne de caractères.
     */
    public static String read(String path) {
        final var uri = expect(() -> ResourceExt.class.getClassLoader().getResource(path).toURI(), "resource should exist");
        return expect(() -> Files.readString(Paths.get(uri)), "resource should be readable");
    }

}