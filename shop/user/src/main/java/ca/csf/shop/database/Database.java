package ca.csf.shop.database;

import ca.csf.shop.util.ResourceExt;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class Database {
    private final Connection db;

    public Database(String path) throws IOException, SQLException {
        db = connect(path);
        migrate(db);
    }

    private static Connection connect(String path) throws IOException, SQLException {
        final var dirPath = Paths.get(path).getParent();
        if (dirPath != null) {
            final var dirFile = dirPath.toFile();
            if (!dirFile.exists()) {
                if (!dirFile.mkdirs()) {
                    throw new IOException("Unable to create directory at " + dirPath + ".");
                }
            }
        }

        return DriverManager.getConnection(String.format("jdbc:sqlite:%s", path));
    }

    private static void migrate(Connection db) throws SQLException {
        db.createStatement().execute(ResourceExt.read("migrations/initial.sql"));
    }

    public Connection connection() {
        return db;
    }

    public void begin() throws SQLException {
        db.setAutoCommit(false);
    }

    public void commit() throws SQLException {
        db.commit();
        db.setAutoCommit(true);
    }

    public void rollback() throws SQLException {
        db.rollback();
        db.setAutoCommit(true);
    }
}
