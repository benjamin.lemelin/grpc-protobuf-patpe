package ca.csf.shop.api;

import ca.csf.shop.database.Database;
import ca.csf.shop.repository.UserEntity;
import ca.csf.shop.repository.UserRepository;
import ca.csf.shop.user.User;
import ca.csf.shop.user.UserServiceGrpc;
import ca.csf.shop.crypto.Argon2ID;
import ca.csf.shop.util.StringExt;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;

import java.sql.SQLException;

public final class UserService extends UserServiceGrpc.UserServiceImplBase {
    private final Database db;
    private final UserRepository userRepository;
    private final Argon2ID argon2id;

    public UserService(Database db, UserRepository userRepository) {
        this.db = db;
        this.userRepository = userRepository;
        this.argon2id = new Argon2ID();
    }

    @Override
    public void create(User.UserCreateRequest request, StreamObserver<User.UserCreateResponse> response) {
        var email = request.getEmail().trim();
        if (email.isEmpty()) {
            response.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("email cannot be empty")));
            return;
        } else if (!StringExt.isEmail(email)) {
            response.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("email is invalid")));
            return;
        }

        var password = request.getPassword();
        if (password.isEmpty()) {
            response.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("password cannot be empty")));
            return;
        }

        UserEntity user = new UserEntity(email, argon2id.encode(password));
        try {
            db.begin();

            user = userRepository.insertOne(user);
            if (user == null) {
                db.rollback();
                response.onError(new StatusException(Status.FAILED_PRECONDITION.withDescription("user already exists")));
                return;
            }

            db.commit();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                db.rollback();
            } catch (SQLException e2) {
                System.err.println(e2.getMessage());
            }
            response.onError(new StatusException(Status.INTERNAL));
            return;
        }

        response.onNext(User.UserCreateResponse.newBuilder().build());
        response.onCompleted();
    }

    @Override
    public void login(User.UserLoginRequest request, StreamObserver<User.UserLoginResponse> response) {
        var email = request.getEmail().trim();
        if (email.isEmpty()) {
            response.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("email cannot be empty")));
            return;
        } else if (!StringExt.isEmail(email)) {
            response.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("email is invalid")));
            return;
        }

        var password = request.getPassword();
        if (password.isEmpty()) {
            response.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("password cannot be empty")));
            return;
        }

        try {
            var user = userRepository.findByEmail(email);
            if (user == null) {
                response.onError(new StatusException(Status.NOT_FOUND.withDescription("user not found")));
                return;
            }

            if (!argon2id.verify(password, user.password())) {
                response.onError(new StatusException(Status.NOT_FOUND.withDescription("user not found")));
                return;
            }

            response.onNext(User.UserLoginResponse.newBuilder().build());
            response.onCompleted();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            response.onError(new StatusException(Status.INTERNAL));
        }
    }
}
