package ca.csf.shop.repository;

import ca.csf.shop.database.Database;
import org.sqlite.SQLiteErrorCode;
import org.sqlite.SQLiteException;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;

public final class UserRepository {
    private final Connection db;

    public UserRepository(Database db) {
        this.db = db.connection();
    }

    @Nullable
    public UserEntity findByEmail(String email) throws SQLException {
        final var statement = db.prepareStatement("""
            SELECT
                id,
                email,
                password
            FROM
                user
            WHERE
                email = ?"""
        );
        statement.setString(1, email);

        final var result = statement.executeQuery();
        if (result.next()) {
            return new UserEntity(
                result.getInt(1),
                result.getString(2),
                result.getString(3)
            );
        } else {
            return null;
        }
    }

    public UserEntity insertOne(UserEntity entity) throws SQLException {
        final var statement = db.prepareStatement("""
            INSERT INTO user
                (email, password)
            VALUES
                (?,?)"""
        );
        statement.setString(1, entity.email());
        statement.setString(2, entity.password());

        try {
            statement.executeUpdate();

            final var result = statement.getGeneratedKeys();
            if (result.next())
                return entity.withId(result.getInt(1));
            else
                return null;
        } catch (SQLiteException e) {
            if (e.getResultCode().code == SQLiteErrorCode.SQLITE_CONSTRAINT_UNIQUE.code) return null;
            else throw e;
        }
    }
}
