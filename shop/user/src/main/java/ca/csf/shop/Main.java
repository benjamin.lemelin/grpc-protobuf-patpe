package ca.csf.shop;

import ca.csf.shop.api.UserService;
import ca.csf.shop.database.Database;
import ca.csf.shop.repository.UserRepository;
import io.grpc.ServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionServiceV1;

import java.io.IOException;
import java.sql.SQLException;

import static ca.csf.shop.util.ExceptionExt.expect;

public final class Main {

    public static void main(String[] args) throws IOException, SQLException {
        var database = new Database(".shop/app.db");
        var userRepository = new UserRepository(database);

        var builder = ServerBuilder
            .forPort(5001)
            .addService(new UserService(database, userRepository));

        if (System.getenv("dev") != null) builder.addService(ProtoReflectionServiceV1.newInstance());

        var server = builder.build();

        System.out.println("Server is running");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> expect(() -> server.shutdown().awaitTermination(), "server should stop without issue")));
        expect(() -> server.start().awaitTermination(), "server should start without issue");
    }

}
