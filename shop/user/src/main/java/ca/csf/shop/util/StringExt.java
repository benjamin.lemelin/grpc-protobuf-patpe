package ca.csf.shop.util;

import java.util.regex.Pattern;

/**
 * Utilitaires pour les chaines de caractères.
 */
public final class StringExt {

    /**
     * Expression régulière permettant de valider une adresse e-mail.
     */
    private static final Pattern EMAIL_REGEX = Pattern.compile("[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,}");

    /**
     * Constructeur privé pour empêcher l'instanciation de cette classe utilitaire.
     */
    private StringExt() {
        // Privé pour empêcher l'instanciation.
    }

    /**
     * Vérifie si une chaîne de caractères est une adresse e-mail valide.
     *
     * @param value La chaîne de caractères à vérifier.
     * @return {@code true} si la chaîne correspond à une adresse e-mail valide, {@code false} sinon.
     */
    public static boolean isEmail(String value) {
        return EMAIL_REGEX.matcher(value).matches();
    }

}