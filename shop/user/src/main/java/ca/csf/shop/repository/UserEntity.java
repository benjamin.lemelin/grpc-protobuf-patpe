package ca.csf.shop.repository;

public record UserEntity(
    Integer id,
    String email,
    String password
) {

    public UserEntity(String email, String password) {
        this(null, email, password);
    }

    public UserEntity withId(Integer id) {
        return new UserEntity(id, email, password);
    }

}
