CREATE TABLE IF NOT EXISTS user
(
    id          INTEGER     NOT NULL PRIMARY KEY AUTOINCREMENT,
    email       TEXT        NOT NULL UNIQUE,
    password    TEXT        NOT NULL
)