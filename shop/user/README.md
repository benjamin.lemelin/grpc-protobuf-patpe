# User Service

Service gRPC pour les utilisateurs (rédigé en Java).

## Démarrage rapide

Ce projet nécessite d'installer le [Java Development Kit][JDK] sur votre machine. Veuillez suivre les instructions en
fonction de votre système d'exploitation. Une fois fait, vous pourrez alors exécuter le projet :

```shell
./gradlew run
```

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../../LICENSE.md) pour les détails.

[JDK]: https://openjdk.org/