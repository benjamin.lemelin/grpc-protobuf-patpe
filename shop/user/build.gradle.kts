val javaxVersion = "1.3.2"
val protobufVersion = "4.29.1"
val grpcVersion = "1.68.2"
val grpcNettyVersion = "1.68.2"
val grpcJavaVersion = "1.70.0"
val argon2Version = "1.80"
val sqliteJdbcVersion = "3.49.1.0"

plugins {
    id("java")
    id("com.google.protobuf") version "0.9.4"
}

group = "ca.csf"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation("javax.annotation:javax.annotation-api:$javaxVersion")
    implementation("com.google.protobuf:protobuf-java:$protobufVersion")
    implementation("io.grpc:grpc-api:$grpcVersion")
    implementation("io.grpc:grpc-protobuf:$grpcVersion")
    implementation("io.grpc:grpc-services:$grpcVersion")
    implementation("io.grpc:grpc-netty:$grpcNettyVersion")
    implementation("io.grpc:grpc-stub:$grpcJavaVersion")

    implementation("org.bouncycastle:bcprov-jdk18on:$argon2Version")
    implementation("org.xerial:sqlite-jdbc:$sqliteJdbcVersion")
}

sourceSets {
    main {
        proto {
            srcDir("../.proto")
            include("../.proto")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
    plugins {
        create("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                create("grpc")
            }
        }
    }
}

task("run", JavaExec::class) {
    environment("dev", "true")

    mainClass = "ca.csf.shop.Main"
    classpath = sourceSets["main"].runtimeClasspath
}