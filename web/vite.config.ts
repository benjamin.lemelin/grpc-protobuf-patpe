import { defineConfig } from "vite";
import SolidPlugin from "vite-plugin-solid";

export default defineConfig({
  server: {
    port: 3000,
    proxy: {
      "/api": {
        target : "http://127.0.0.1:5000/",
        rewrite: path => path.replace(/^\/api/, ""),
      }
    },
  },
  css: {
    modules: {
      localsConvention: "camelCase"
    },
  },
  plugins: [
      SolidPlugin()
  ],
  build: {
    target: "esnext",
  },
});
