import {JSX, Match, Resource, Switch} from "solid-js";

type Props<T, E> = {
    resource: Resource<T>,
    success: JSX.Element | ((ressource: T) => JSX.Element),
    error: JSX.Element | ((value: E) => JSX.Element),
    loading?: JSX.Element,
    fallback?: JSX.Element,
}

export function Suspense<T, E>(props: Props<T, E>) {
    // We are not using Solid's suspense, because fallbacks are not shown when a refresh is requested,
    // while this version does. Also, this component simplifies error handling, so there is less code
    // on call side (no more <Show> inside a <Suspense> inside an <ErrorBoundary> ...).
    return (
        <Switch fallback={props.fallback}>
            <Match when={props.resource.loading} keyed>{
                props.loading
            }</Match>
            <Match when={props.resource.error} keyed>{error => {
                if (typeof props.error === "function") return props.error(error);
                else return props.error;
            }}</Match>
            <Match when={props.resource()} keyed>{ressource => {
                if (typeof props.success === "function") return props.success(ressource);
                else return props.success;
            }}</Match>
        </Switch>
    );
}