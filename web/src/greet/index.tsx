import {createSignal, Show} from "solid-js";
import {GreeterClient, GreetRequest, GreetResponse, GrpcWebTransport, RpcError} from "../api";

import Style from './style.module.css';
import {Spinner} from "../spinner";

// Instanciation du client.
const client = new GreeterClient(new GrpcWebTransport({baseUrl: "/api"}));

type Props = {
    class? : string
}

export function Greet(props : Props) {
    // Signal représentant la donnée contenue dans le champ du formulaire. Il s'agit d'une structure du framework Solid.
    const [name, setName] = createSignal("");
    // Signal représentant la réponse en provenance du serveur.
    const [result, setResult] = createSignal<string>();
    // Signal représentant l'erreur reçue par le serveur.
    const [error, setError] = createSignal<string>();
    // Signal indiquant si la requête est en cours.
    const [loading, setLoading] = createSignal(false);

    // Fonction appelée lorsque le formulaire est envoyé.
    async function onFormSubmit(e: SubmitEvent) {
        // Empêche de rafraichir la page.
        e.preventDefault();

        // Affiche la barre de progression.
        setLoading(true);

        // Création de la requête.
        const request: GreetRequest = {name: name()};

        // Envoi de la requête.
        try {
            const response: GreetResponse = await client.greet(request).response;
            setResult(response.result);
            setError();
        } catch (e) {
            setResult();
            if (e instanceof RpcError) setError(decodeURI(e.message));
            else setError(`${e}`);
        }

        // Cache la barre de progression.
        setLoading(false);
    }

    // Affichage (éléments HTML).
    return (
        <main class={`${Style.main} ${props.class ?? ""}`}>
            <header class="muted">
                Entrez votre nom et appuyez sur <em>Envoyer</em> pour être salué par le service <em>gRPC</em>.
            </header>

            <form class={Style.form} on:submit={onFormSubmit}>
                <input class={Style.input}
                       type="text"
                       value={name()}
                       placeholder="Votre nom"
                       on:input={e => setName(e.currentTarget.value)}/>
                <button class={Style.button}>
                    Envoyer
                </button>
            </form>

            <Show when={result()} keyed>{result =>
                <div class={Style.result}>{result}</div>
            }</Show>

            <Show when={error()} keyed>{error =>
                <div class={Style.error}>{error}.</div>
            }</Show>

            <Show when={loading()} keyed>{ _ =>
                <div class={Style.loading}><Spinner/></div>
            }</Show>
        </main>
    );
}
