import {HeaderBar} from "../headerbar";
import {Greet} from "../greet";

import Style from './style.module.css';
import Background from "./background.jpg";

export function App() {
    return (
        <>
            <HeaderBar class={Style.header}/>
            <Greet class={Style.main}/>
            <img class={Style.background} src={Background} alt="Clavier d'ordinateur"/>
        </>
    );
}
