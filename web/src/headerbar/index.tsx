import Style from "./style.module.css";

type Props = {
    class? : string
}

export function HeaderBar(props : Props) {
    return (
        <header class={`${Style.headerbar} ${props.class ?? ""}`}>
            <h1>Hello gRPC Web</h1>
        </header>
    );
}