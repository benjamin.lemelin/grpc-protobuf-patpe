/* @refresh reload */

// Import base Css before any other Css file to facilitate style redefinitions.
import "./index.css";

// Import Solid and App component.
import {render} from "solid-js/web";
import {App} from "./app";

render(
    () => <App/>,
    document.getElementById("root") as HTMLElement
);
