export {RpcError} from "@protobuf-ts/runtime-rpc";
export {GrpcWebFetchTransport as GrpcWebTransport} from "@protobuf-ts/grpcweb-transport";

export {Greeter, GreetRequest, GreetResponse} from "./hello_pb";
export {GreeterClient} from "./hello_pb.client";
