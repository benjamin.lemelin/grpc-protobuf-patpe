![Logo Firefox](../.docs/firefox-logo.svg)

# Web

Ce projet expérimente avec GRPC et Protobuf en JavaScript sur un navigateur web.

## Démarrage rapide

Ce projet nécessite d'installer [Node][Node Runtime] sur votre machine. Veuillez suivre les instructions en fonction de
votre système d'exploitation. Si vous aviez déjà le Node sur votre machine, il vous est recommandé de le mettre à jour 
à sa dernière version LTS avant de débuter.

### Windows

Téléchargez et exécutez l'installateur de [Node][Node Windows Install] pour Windows. Suivez les instructions.

### Ubuntu et dérivés

Installez Node avec votre gestionnaire de paquets :

```shell
sudo apt install nodejs
```

### Fedora

Installez Node avec votre gestionnaire de paquets :

```shell
sudo dnf install nodejs
```

### MacOs

Utilisez [Homebrew] pour installer Node :

```shell
brew install node@22
```

## Exécuter le projet

Avant toute chose, vous devez compiler manuellement les fichiers `.proto` pour le web. Vous pouvez le faire en utilisant
cette commande (à l'intérieur du dossier `web`) :

```shell
npm run proto
```

Ensuite, démarrez le projet web (incluant son serveur de développement) avec la commande ci-dessous. Vous devriez pouvoir
accéder à la page web en suivant [ce lien](http://localhost:3000/).

```shell
npm run dev
```

Dans un autre terminal, démarrez un serveur gRPC. Vous pouvez choisir l'implémentation que vous désirez, tel que
[C#](../csharp/README.md) ou [Rust](../rust/README.md). En cas de doutes, prenez l'implémentation en Rust. Par exemple,
voici la commande à exécuter pour démarrer le serveur en Rust :

```shell
cargo run
```

## Environnements de développement

Il est recommandé d'utiliser [IntelliJ IDEA] en guise d'IDE. Vous pouvez aussi utiliser [Visual Studio Code].

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](../LICENSE.md) pour les détails.

[Node Runtime]: https://nodejs.org/
[Node Windows Install]: https://nodejs.org/fr/download/prebuilt-installer
[IntelliJ IDEA]: https://www.jetbrains.com/idea/
[Visual Studio Code]: https://code.visualstudio.com/
[Homebrew]: https://brew.sh/