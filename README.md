![Logo gRPC](.docs/grpc-logo.png)

# gRPC et Protobuf - Expérimentations

Expérimentations avec gRPC (gRPC Remote Procedure Call) et Protocol Buffers (Protobuf). Il s'agit de technologies 
développées par Google pour permettre une communication efficace et rapide entre des applications.

Ce dépôt contient des projets dans divers langages. La majorité des projets contiennent un client et un server gRPC. 
Tous les clients et les serveurs sont interchangeables, ce qui veut dire que vous pouvez démarrer un serveur en 
[Rust](rust/README.md) et l'appeler avec un client en [C#](csharp/README.md), et vice-versa.

## Démarrage rapide

Ce dépôt contient plusieurs projets distincts. Pour commencer, tous les projets nécessitent le compilateur [Protobuf]. 
Suivez ces instructions pour installer le compilateur selon votre système d'exploitation. Notez que la majorité des 
projets ont aussi des dépendances spécifiques. Consultez leurs fichiers `README.md` individuellement pour les détails.

### Windows

Téléchargez la dernière version sur le site de [Github][Protobuf-Github-Releases] (par exemple, `protoc-29.3-win64.zip`).
Placez le contenu de l'archive à un endroit convenable (tel que `%USERPROFILE%\.protoc`). Finalement, assurez-vous 
que le dossier `bin` soit inclut dans votre variable d'environnement `%PATH%` (ajoutez `%USERPROFILE%\.protoc\bin`).

### Ubuntu et dérivés

Installez le compilateur et ses dépendances en utilisant le *package manager* de votre système :

```shell
sudo apt update
sudo apt install -y protobuf-compiler libprotobuf-dev
```

### Fedora

Installez le compilateur et ses dépendances en utilisant le *package manager* de votre système :

```shell
sudo dnf install -y protobuf-compiler protobuf-devel
```

### macOS

Utilisez [Homebrew] pour installer le compilateur :

```shell
brew install protobuf
```

## Concepts

gRPC est un framework RPC ([Remote Procedure Call][Remote Procedure Call - Wikipédia]). Ce protocole permet de faire
des appels de procédure à des ordinateurs distants, similaire à ce que nous permettent les API [REST][REST - Wikipédia]
de nos jours.

### Services

Un service est une collection de procédures. Ces procédures peuvent être appelées par des clients distants. Les services
sont définis comme suit dans un fichier `.proto`. Dans cet exemple, le service se nomme `Calculator` et contient deux
procédures, `Add` et `Divide`, acceptant un objet `Request` et retournant un objet `Response`.

```protobuf
service Calculator {
  rpc Add(AddRequest) returns (AddResponse);
  rpc Divide(DivideRequest) returns (DivideResponse);
}
```

Remarquez que le service ne contient aucun code. Il s'agit plutôt d'une interface qui doit être implémenté dans un
langage de programmation cible comme C++, C#, Go, Kotlin, Python, Rust et même PHP. C'est ce qui permet à gRPC de
définir des contrats bien précis, utilisables dans une variété de langages, et vérifiés dès la compilation du projet.

### Messages

Les messages définissent les structures de données échangées entre le client et le serveur. Ils sont spécifiés dans le
fichier `.proto` et traduits en classes ou structures dans le langage de programmation utilisé. L'exemple ci-dessous
contient deux messages, `AddRequest` et `AddResponse`. Chaque champ du message doit posséder un type, un nom et un
numéro (afin de l'identifier de manière unique).

```protobuf
message AddRequest {
  sint64 lhs = 1;
  sint64 rhs = 2;
}

message AddResponse {
  sint64 result = 1;
}
```

### Génération de code

Le compilateur pour les fichiers `.proto` (nommé `protoc`) permet de générer du code dans plusieurs langages de
programmation, servant de point de départ pour la création des véritables services. Dans la majorité des cas, il ne
reste qu'à écrire le code des procédures : la sérialisation, la désérialisation et même les requêtes réseau sont déjà
implémentées dans le code généré.

Voici un exemple en C#. La classe `CalculatorBase` a été générée automatiquement, et tout ce qu'il reste à faire, c'est
d'implémenter les méthodes des services.

```csharp
public class CalculatorService : CalculatorBase
{
    public override async Task<AddResponse> Add(AddRequest request, ServerCallContext context)
    {
        var result = request.Lhs + request.Rhs;
        return new AddResponse() 
        {
            Result = result
        };
    }
    
    public override async Task<DivideResponse> Divide(DivideRequest request, ServerCallContext context)
    {
        if (request.Rhs == 0)
            throw new RpcException(new Status(StatusCode.InvalidArgument, "cannot divide by 0"));
        
        var result = request.Lhs / request.Rhs;
        return new DivideResponse() 
        {
            Result = result
        };
    }
}
```

### Réflection

Pour faciliter le développement, la majorité des implémentations de gRPC offrent un mécanisme de réflection. Cela permet
à des outils comme [gRPCurl] ou [gRPCui] de lister les services offerts par le serveur et d'afficher de la documentation.
Notez par contre que cette fonctionnalité ne devrait être activée que pour le débogage.

## Différences avec les services REST

Les services gRPC sont comparables en termes de fonctionnalités à des services REST, mais il y a des nuances.

### Actions et Ressources

Un service gRPC expose des actions, tandis qu'un service REST expose des ressources. Il s'agit d'une nuance importante.
Voyons ce que cela veut dire.

Pour accéder à un service REST, il faut premièrement fournir une URL vers une resource, tel que `/api/users`. Il faut
ensuite spécifier le bon verbe *HTTP* (`GET`, `POST`, `PUT`, `DELETE`) en fonction de l'action que l'on désire faire sur
cette ressource.

Dans le cas d'un service gRPC, l'ordre est en quelque sorte inversé. Il faut indiquer premièrement l'action à effectuer
en fournissant le nom complet de la procédure à appeler, incluant l'espace de nom. Nous pouvons penser à quelque chose
comme `accounts.UserService.addUser` (il est correct de comparer cela à une URL). La ressource accédée dépend de
l'action effectuée et des paramètres reçus. L'appel de la procédure peut ensuite modifier l'état du système ou retourner
une information selon l'action effectuée (`accounts.UserService.getUser` vs `accounts.UserService.addUser`).

Cela ne veut pas dire qu'un service gRPC ne peut pas offrir l'accès à des ressources, ou qu'un service REST ne peut pas
offrir des actions. Les deux peuvent faire les mêmes choses : c'est l'implémentation qui change. Au final, cela reste
tout de même très semblable.

### Protobuf et Json

gRPC utilise les [Protocol Buffers][Protobuf] en guise de format pour le transfert des données, tandis que les services
REST utilisent généralement le format [JSON]. Il s'agit de l'un des plus gros avantages qu'offre gRPC comparativement à
des services REST traditionnels. Les *Protocol Buffers* ont l'avantage d'avoir un schéma précis, ce qui en facilite la
validation. Le format *JSON*, quant à lui, n'a pas de schéma par défaut, ce qui oblige souvent à faire de nombreuses
validations (bien que cela ne soit pas impossible, voir [Json Schema](https://json-schema.org/)).

Les *Protocol Buffers* sont aussi très performants, autant sur l'espace que sur le temps. N'étant pas destinés à être lu
par des humains, ils sont beaucoup moins lourds à transférer sur le réseau. Ils sont aussi beaucoup plus rapides à
désérialiser, simplement dû au fait que le format n'est pas auto-descriptif (le schéma est dans les fichiers `.proto`).

Un format est auto-descriptif lorsqu'il contient les clés autant que les valeurs. Considérez ce fichier JSON :

```json
{
  "name": "Alice",
  "age": 25,
  "isStudent": false
}
```

Ici, `name` est la clé et `Alice` est la valeur. Le format contient donc le nom des données, ce que le rend justement
plus facilement lisible par un être humain. À l'inverse, les *Protocol Buffers* ne contient pas le nom des données, et
ressemblent plus à ceci (simplifié) :

```text
00 0A 00 05 41 6C 69 63 65 01 25 02 00
```

Bien que les avantages des [Protocol Buffers] soient nombreux, le format [JSON] a bien entendu toujours sa place. Les
navigateurs, entre autres, ne supportent pas les [Protocol Buffers] par défaut, mais ils supportent toujours très bien
le format [JSON] (et sont très rapides à le désérialiser). Pour ces raisons, les services gRPC sont souvent utilisés à
l'interne au lieu de l'externe, afin de communiquer entre différents sous-services au sein de l'entreprise (par exemple,
pour communiquer avec un sous-service d'authentification). Ils sont aussi utilisés dans les applications mobiles
natives, qui peuvent plus facilement se permettre d'utiliser un format différent du [JSON].

### Génération de code

Tel que mentionné plus haut, gRPC fait un usage intensif (et obligatoire) de la génération de code. Ce code est généré
à partir des fichiers `.proto`, autant pour le code client que le code serveur. Cela permet d'établir un contrat clair
entre les deux, et ainsi *garantir* dès la compilation que le format des données et les services offerts sont
correctement définis (sous certaines conditions).

Bien que la génération de code soit aussi possible dans le monde des services REST (voir [OpenAPI]), la pratique n'en
reste pas moins marginale. L'outil est surtout utilisé pour documenter les services (avec [Swagger]) à partir du code
écrit par les développeurs, ce qui est l'inverse avec gRPC.

## Avantages et désavantages : Quand et pourquoi ?

Pour la majorité des systèmes exposés à l'internet, les services REST sont encore d'actualité. Les *frameworks* comme
[Ktor], [Django], [Gin], [Axum], [Laravel] et même [Spring] sont très puissants, stables et bien supportés. Il n'y a 
aucune raison de passer à gRPC pour la partie exposée à l'internet.

En revanche, si vos services effectuent de nombreux appels entre eux à l'interne (la partie qui n'est pas exposée à 
l'internet), alors une technologie comme gRPC devient rapidement très avantageuse : la latence est très basse, les 
messages très petits et compacts, il est possible de créer des *flux* de messages, de communiquer de manière 
bidirectionnelle, et par-dessus tout, d'utiliser le langage le mieux adapté aux besoins du service.

C'est aussi très intéressant pour l'internet des objets. Les [Protobuf] sont plus rapides à désérialiser que du JSON, et
sont donc mieux adaptés aux systèmes embarqués où la puissance de calcul et la mémoire sont limités.

## gRPCurl

[gRPCurl] est un outil similaire à `curl` pour faire des requêtes à un serveur *gRPC*. Il vous est recommandé de 
l'installer pour faciliter le développement et les tests.

Voici un exemple d'utilisation de [gRPCurl] :

```shell
grpcurl -plaintext \
        -proto ./proto/hello.proto \
        -d '{ \"name\" : \"John Smith\" }' \
        127.0.0.1:5000 \
        hello.Greeter.Greet
```

Dans cette commande :

* `-plaintext` : Requête sans *tls*.
* `-proto ./proto/hello.proto` : Fichier `.proto` contenant le format des messages.
* `-d { "name" : "John Smith" }'` : Données de la requête (sous format JSON).
* `"127.0.0.1:5000"` : Hôte cible (avec le port).
* `hello.Greeter.Greet` : Appel de la méthode `Greet` du service `Greeter` (dans le namespace `hello`).

Si le serveur implémente la reflection gRPC (ce que la majorité font), vous pouvez omettre le fichier `.proto` de votre
requête, car le serveur expose lui-même le schéma. Cela signifie aussi que vous pouvez demander à un tel serveur de 
lister les procédures disponibles. Par exemple :

```shell
grpcurl -plaintext 127.0.0.1:5000 list
```

### Windows

Téléchargez la dernière version sur [Github][gRPCurl-Github-Releases] (par exemple, `grpcurl_1.9.2_windows_x86_64.zip`).
Pour simplifier, vous pouvez placer l'exécutable avec le compilateur `protoc` (tel que `%USERPROFILE%/.protoc/bin`) 
(ce dossier devrait déjà être inclus dans votre variable d'environnement `%PATH%`).

### Linux

Téléchargez le fichier `.tar.gz` le plus récent sur [Github][gRPCurl-Github-Releases] (par exemple, `grpcurl_1.9.2_linux_x86_64.tar.gz`).
Placez le contenu de l'archive avec vos exécutables locaux (tel que `$HOME/.local/bin`). Assurez-vous que ce dossier 
soit inclut dans votre variable d'environnement `$PATH` (ce qui est normalement le cas).

### macOS

Utilisez [Homebrew] pour installer gRPCurl :

```shell
brew install grpcurl
```

## gRPC UI

[gRPCui] est un outil permettant d'interagir avec des serveurs gRPC via un navigateur, à la manière de [Postman], mais 
pour un API gRPC au lieu d'un API REST. Si le serveur implémente la reflection gRPC, il est même en mesure d'offrir de
la documentation sur les services offerts par le serveur, et ce, automatiquement.

Pour démarrer gRPCui, exécutez la commande ci-dessous. Le programme devrait normalement ouvrir automatiquement
la page dans votre navigateur. Notez que vous devez fournir l'adresse vers le serveur gRPC que vous désirez utiliser.

```shell
grpcui -plaintext 127.0.0.1:5000
```

### Windows

Téléchargez la dernière version sur [Github][gRPCui-Github-Releases] (par exemple, `grpcui_1.4.2_windows_x86_64.zip`).
Pour simplifier, vous pouvez placer l'exécutable avec le compilateur `protoc` (soit que `%USERPROFILE%/.protoc/bin`)
(ce dossier devrait déjà être inclut dans votre variable d'environnement `%PATH%`).

### Linux

Téléchargez le fichier `.tar.gz` le plus récent sur [Github][gRPCui-Github-Releases] (par exemple, `grpcui_1.4.2_linux_x86_64.tar.gz`).
Placez le contenu de l'archive avec vos exécutables locaux (tel que `$HOME/.local/bin`). Assurez-vous que ce dossier
soit inclut dans votre variable d'environnement `$PATH` (ce qui est normalement le cas).

### macOS

Utilisez [Homebrew] pour installer gRPCurl :

```shell
brew install grpcui
```

## Environnements de développement

Cette section contient diverses recommandations pour améliorer le développement en fonction de votre *IDE* favori.

### IntelliJ IDEA et dérivés

Installez le plugin [Protocol Buffers](https://plugins.jetbrains.com/plugin/14004-protocol-buffers) de Jetbrains. Il fournit de la coloration syntaxique ainsi que de 
l'autocomplétion.

### Visual Studio Code

L'extension [vscode-proto3](https://marketplace.visualstudio.com/items?itemName=zxh404.vscode-proto3) semble la plus populaire pour offrir de l'autocomplétion.

## Licence

Ce dépôt est sous licence MIT. Consultez le fichier [LICENSE.md](LICENSE.md) pour les détails.

[Protobuf]: https://protobuf.dev/

[Homebrew]: https://brew.sh/
[Open API]: https://swagger.io/specification/
[JSON]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON
[Swagger]: https://swagger.io/
[Postman]: https://www.postman.com/
[gRPCurl]: https://github.com/fullstorydev/grpcurl
[gRPCui]: https://github.com/fullstorydev/grpcui

[REST - Wikipédia]: https://fr.wikipedia.org/wiki/Representational_state_transfer
[Remote Procedure Call - Wikipédia]: https://fr.wikipedia.org/wiki/Appel_de_proc%C3%A9dure_%C3%A0_distance

[Protobuf-Github-Releases]: https://github.com/protocolbuffers/protobuf/releases
[gRPCurl-Github-Releases]: https://github.com/fullstorydev/grpcurl/releases
[gRPCui-Github-Releases]: https://github.com/fullstorydev/grpcui/releases

[Ktor]: https://ktor.io/
[Django]: https://www.djangoproject.com/
[Gin]: https://gin-gonic.com/
[Axum]: https://github.com/tokio-rs/axum
[Laravel]: https://laravel.com/
[Spring]: https://spring.io/